//
//  Interaction.hpp
//  LiquidParticleMixture
//
//  Created by Ryohei Seto on 3/19/16.
//  Copyright © 2016 T. I. ZOHDI, Ryohei Seto. All rights reserved.


#ifndef Interaction_hpp
#define Interaction_hpp

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include "vec3d.h"

class System;

class Interaction{
private:
	/*********************************
	 *        Members                *
	 *********************************/
	System *sys;
	double a0; // radii
	double a1; // second raddi > a0
	double ro_12; // ro_12 = ro/2
	//======= internal state =====================//
	bool active;
	unsigned int label;
	unsigned int p0;
	unsigned int p1;
	//======= relative position/velocity data  =========//
	//double reduced_gap; // gap between particles (dimensionless gap = s - 2, s = 2r/(a1+a2) )
	vec3d relative_velocity;
	vec3d rolling_velocity;
	//===== forces and stresses ==================== //
	double interaction_range;  // max distance
	/*********************************
	 *       Private Methods         *
	 *********************************/
	//===== forces and stresses computations =====//
	inline void set_ro(double val)
	{
		ro = val; // ro = a0 + a1
		ro_12 = ro/2;
	};
	//void calcNormalVectorDistanceGap();
	void calcRcalelativeVelocities();
	void calcRollingVelocities();
	void integrateStress();
	vec3d calcContactForce(std::vector<vec3d> &position);
	//===== forces/stresses  ========================== //
	/* To avoid discontinous change between predictor and corrector,
	 * the change of contact state is informed in updateResiCoeff.
	 */
	bool contact_state_changed_after_predictor;
	
public:
	vec3d relative_surface_velocity;
	double ro; // ro = a0+a1;
	double r; // center-center distance
	vec3d rvec; // vector center to center
	vec3d nvec; // normal vector
	vec3d contact_force_t;
	vec3d contact_force_t_p_dt;
	/*********************************
	 *       Public Methods          *
	 *********************************/
	Interaction(){}
	void init(System *sys_);
	//======= state updates  ====================//
	/* Update the follow items:
	 * - r_vec, zshift, _r, and nr_vec
	 * - contact_velocity_tan
	 * - disp_tan
	 * - Fc_normal and Fc_tan
	 * - check breakup of static friction
	 * - State (deactivation, contact)
	 */
	void updateState(bool& deactivated);
	void updateContactState();
	void activate(unsigned int i, unsigned int j, double interaction_range_);
	void deactivate();
	void calcNormalVectorDistanceGap(std::vector<vec3d> &position);
	
	inline vec3d relative_surface_velocity_direction() {
		return relative_surface_velocity/relative_surface_velocity.norm();
	}
	inline bool is_overlap()
	{
		return r < ro;
	}
	inline bool is_active()
	{
		return active;
	}
	//======= particles data  ====================//
	inline int partner(unsigned int i)
	{
		return (i == p0 ? p1 : p0);
	}
	inline void	get_par_num(unsigned int& i, unsigned int& j)
	{
		i = p0, j = p1;
	}
	inline void set_label(unsigned int val)
	{
		label = val;
	}
	inline unsigned int get_label()
	{
		return label;
	}
	//======= relative position/velocity  ========//
	inline double get_ro_r()
	{
		return ro/r;
	}
	//	inline double get_reduced_gap()
	//	{
	//		return reduced_gap;
	//	}
	inline double get_gap()
	{
		return r-ro;
	}
	double getNormalVelocity();
	double getRelativeVelocity()
	{
		return relative_velocity.norm();
	}
	double getContactVelocity();
	
};

#endif /* Interaction_hpp */
