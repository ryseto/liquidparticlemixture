//
//  Interaction.cpp
//  LiquidParticleMixture
//
//  Created by Ryohei Seto on 3/19/16.
//  Copyright © 2016 Ryohei Seto. All rights reserved.
//

#include "Interaction.hpp"
#include "System.hpp"


void Interaction::init(System* sys_)
{
	sys = sys_;
	active = false;
}


/* Make a normal vector
 * Periodic boundaries are checked for all partices.
 * vector from particle 0 to particle 1. ( i --> j)
 * pd_z : Periodic boundary condition
 */
void Interaction::calcNormalVectorDistanceGap(std::vector<vec3d> &position)
{
	rvec = position[p1]-position[p0];
	sys->periodize_diff(rvec);
	r = rvec.norm();
	nvec = rvec/r;
	//	reduced_gap = r/ro_12-2;
}

/* Activate interaction between particles i and j.
 * Always j>i is satisfied.
 */
void Interaction::activate(unsigned int i, unsigned int j,
						   double interaction_range_)
{
	active = true;
	if (j > i) {
		p0 = i, p1 = j;
	} else {
		p0 = j, p1 = i;
	}
	// tell it to particles i and j
	sys->interaction_list[i].insert(this);
	sys->interaction_list[j].insert(this);
	// tell them their new partner
	sys->interaction_partners[i].insert(j);
	sys->interaction_partners[j].insert(i);
	//
	//sys->updateNumberOfInteraction(p0, p1, 1);
	//
	a0 = sys->radius[p0];
	a1 = sys->radius[p1];
	/* [note]
	 * The reduced (or effective) radius is defined as
	 * 1/a_reduced = 1/a0 + 1/a1
	 * This definition comes from sphere vs half-plane geometory of contact mechanics.
	 * If sphere contacts with a half-plane (a1 = infinity), a_reduced = a0.
	 * For equal sized spheres, a_reduced = 0.5*a0 = 0.5*a1
	 */
	//a_reduced = a0*a1/(a0+a1);
	set_ro(a0+a1); // ro=a0+a1
	interaction_range = interaction_range_;
	/* NOTE:
	 * lub_coeff_contact includes kn.
	 * If the scaled kn is used there,
	 * particle size dependence appears in the simulation.
	 * I don't understand this point yet.
	 * lub_coeff_contact_scaled = 4*kn_scaled*sys->contact_relaxation_time;
	 */
	calcNormalVectorDistanceGap(sys->particle_position_t);
}

void Interaction::deactivate()
{
	// r > interaction_range
	active = false;
	sys->interaction_list[p0].erase(this);
	sys->interaction_list[p1].erase(this);
	sys->interaction_partners[p0].erase(p1);
	sys->interaction_partners[p1].erase(p0);
}

vec3d Interaction::calcContactForce(std::vector<vec3d> &position)
{
	vec3d force(0,0,0);
	calcNormalVectorDistanceGap(position);
	double epsilon = (r-ro)/ro;
	if (epsilon < 0) {
		double ep_power = epsilon;
		for (int i=1; i<sys->p.contact_force_exponent; i++) {
			ep_power *= epsilon;
		}
		force = -sys->p.contact_force_stiffness*std::abs(ep_power)*nvec;
		//std::cerr << r/ro_12 << std::endl;
	}
	return force;
}

void Interaction::updateState(bool& deactivated)
{
	contact_force_t = calcContactForce(sys->particle_position_t);
	contact_force_t_p_dt = calcContactForce(sys->particle_position_t_p_dt);
	if (r > interaction_range) {
		/* all interactions are switched off. */
		deactivate();
		deactivated = true;
		return;
	}
}

