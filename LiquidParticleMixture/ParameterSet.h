//
//  ParameterSet.h
//  SimulationTemplate
//
//  Created by Ryohei Seto on 3/17/16.
//  Copyright © 2016 T. I. ZOHDI, Ryohei Seto. All rights reserved.
//

#ifndef ParameterSet_h
#define ParameterSet_h

#include <string>

struct ParameterSet
{
	
	double time_end;
	bool uniform_rho;
	bool uniform_mu;
	
	/*******************************************************
	 INTERACTIONS
	 ********************************************************/
	double interparticle_interaction_A1;
	double interparticle_interaction_A2;
	double interparticle_interaction_B1;
	double interparticle_interaction_B2;
	double contact_force_stiffness; // A3
	double contact_force_exponent; // B3
	double neighbor_cutoff_dist;
	double decay_x;
	double decay_y;
	double decay_z;
	double interaction_cutoff;
	double particle_radius;
	double wall_stiffness;
	/*******************************************************
	 INTEGRATOR
	 ********************************************************/
	double phi; // a trapezoidal "phi-scheme", 0 < phi < 1
	double damping; // what is this?
	/*******************************************************
	 OUTPUT
	 ********************************************************/
	
	/*******************************************************
	 Fluid/matrix properties and particle properties
	 ********************************************************/
	int drag_method;
	double coeff_drag;
	double mu_liquid; // [0.1] basic value of liquid viscosity?
	double mu2; // what is this? Viscosity of particle??
	double mu_sens; // what is this? s
	double q_sens;
	double q_liquid;  // what is this?
	double q2; // what is this?
	double q_particle; // charge
	double rho_liquid; // [1000] density of liquid
	double rho_particle; // [5000] density of particle
	double rho2;        // [5000] density of particle
	double rho_face; // (1-vf)*rho_liquid +vf*rho_particle;
	double charge; // electric charge of particle
	int charge_option;
	bool imcompressible;
	double pressure0;
	double friccoeff_dynamics; // MUDYNAMIC
	double friccoeff_static; // MUSTATIC
	double contact_comp; // CONTCOMP
	double inflow_velocity; // VINFLOW
	double outflow_velocity; // VOUTFLOW
	double profile; // ???
	double off; // ?? abbreviation of what?
	/*******************************************************
	 OTHER PARAMETERS
	 ********************************************************/
	int x_mesh; // NX1
	int y_mesh; // NX2
	int z_mesh; // NX2
	bool side_periodic;
	double geometrical_scaling;
	bool motionless;
	double time_extension;
	double majic_scale; // what is this???
	int movie_frames;
	double max_factor; // unclear name
	double min_factor; // unclear name
	double min_tolerance;
	int norm_option; // (for adaptive time step?)
	int readapt; // (for adaptive time step?)
	double expo; // what is this?
	int internal_iterations;
	double couple_tolerance;
	int couple_iteration_limit;
	bool min_override; // what is this?
	double vf; /// [] What is this?
	double np; // [1000] number of particle
	double coupling_weight_flow;
	double coupling_weight_particle;
	double max_scale; // unclear name
	double min_scale;// unclear name
};


#endif /* ParameterSet_h */
