//
//  System.hpp
//  LiquidParticleMixture
//
//  Created by Ryohei Seto on 3/17/16.
//  Copyright © 2016 T. I. ZOHDI, Ryohei Seto. All rights reserved.

#ifndef System_hpp
#define System_hpp

#include <list>
#include <iostream>
#include <iomanip>
#include <vector>
#include <queue>
#include <set>
#include <fstream>
#include "ParameterSet.h"
#include "Global.h"
#include "vec3d.h"
#include "Events.h"
#include "StressTensor.h"
#include "BoxSet.hpp"
#include "Interaction.hpp"

struct intPosition {
	int ix, iy, iz;
};

class System {
private:
	std::list <Event>& events;
	int size_array_double;
	int size_array_vec3d;
	int size_array_StressTensor;
	double rho_face;
	
	double rho_const;
	double mu_const;
	std::vector<intPosition> particle_domain;
	vec3d ***flow_velocity_t;
	vec3d ***flow_velocity_t_p_dt;
	vec3d ***flow_velocity_couple;
	vec3d ***gridforces_t;
	vec3d ***gridforces_t_p_dt;
	vec3d ***grad_rho_t;
	vec3d ***grad_rho_t_p_dt;
	double ***rho_initial; // density of fluid
	double ***rho; // density of fluid
	double ***rho_t; // density of fluid
	double ***rho_t_p_dt; // density of fluid at time t+dt
	double ***mu;
	double ***mu_t;
	double ***mu_t_p_dt;
	double ***mu_initial;
	double ***qf; // what is this?
	double ***qf_initial; // what is this?
	double ***pressure_t;
	double ***pressure_t_p_dt;
	double ***lambda_t;
	double ***lambda_t_p_dt;
	StressTensor ***stress_t_p_dt;
	StressTensor ***sym_velocity_grad_t_p_dt;
	std::vector<double> rho_face_list;
	std::vector<vec3d> velocity_face_list;
	std::vector<double> particle_mass;
	std::vector<double> particle_area;
	std::vector<vec3d> particle_velocity_t_p_dt;
	std::vector<vec3d> particle_velocity_couple;
	std::vector<vec3d> force_t;
	std::vector<vec3d> force_t_p_dt;
	std::vector<vec3d> contact_force_t;
	std::vector<vec3d> contact_force_t_p_dt;
	
	std::vector<intPosition> ipos_t;
	std::vector<intPosition> ipos_t_p_dt;
	//vec3d inflow_vel;
	double dx_mesh;
	double dy_mesh;
	double dz_mesh;
	double dx2[3];
	int pert[3]; // what is this?
	double radius_0;
	bool o_flag; // what is this?
	double const_ts1; // used in reductionTimestep() and enlargeTimestep()
	int num_interaction;
	int maxnum_interaction;
	std::queue<int> deactivated_interaction;
	
	void setCoupleVelocities();
	
	
	void allocate3Dbox_vec3d(vec3d ***&variable, int xsize, int ysize, int zsize);
	void allocate3Dbox_double(double ***&variable, int xsize, int ysize, int zsize);
	void allocate3Dbox_stress(StressTensor ***&variable, int xsize, int ysize, int zsize);
	void ParticleFluidInteraction(int i, intPosition ip_,
								  vec3d &p_position,
								  vec3d &p_velocity,
								  vec3d ***flow_velocity_,
								  double ***rho_,
								  double ***mu_,
								  vec3d &force_,
								  vec3d ***&gridforces_);
	
	void fieldCalculation(int ix, int iy, int iz);
	void fieldCalculationPostprocess(int ix, int iy, int iz, vec3d* diff_velocity_t_p_dt);
	void dragForces();
	void updateInteractions();
	void updateVelocityPositionInIteration();
	void updateFlowVelocity(double *d_stress_t_p_dt, double *d_stress_t,
							vec3d &convective_t_p_dt, vec3d &convective_t,
							int ix, int iy, int iz);
	void calcGradients(vec3d *diff_velocity_t_p_dt, vec3d *diff_velocity_t,
					   vec3d &convective_t_p_dt, vec3d &convective_t, int ix, int iy, int iz);
	
	void mesh_gradient(vec3d &gradient, int ix, int iy, int iz, double ***scalar_field);
	void calc_diff_mu(vec3d *diff_mu, // output
					  double *cross_diff_mu, // output
					  double ***mu_,
					  vec3d ***flow_velocity_,
					  int ix, int iy, int iz);
	void calcViscosityField(vec3d *diff_mu_t_p_dt,
							vec3d *diff_mu_t,
							double *cross_diff_mu_t_p_dt,
							double *cross_diff_mu_t,
							int ix, int iy, int iz);
	void derivativeStress(double *d_stress, vec3d *diff_velocity, vec3d *diff_mu, double *cross_diff_mu_);
	
	void trimingRange(intPosition ip_, int *ix_range, int *iy_range, int *iz_range);
	
	
	bool in_range(int ix, int iy, int iz);
	
public:
	System(ParameterSet& ps, std::list <Event>& ev);
	~System();
	double drag_coeff_search; //@@@@
	ParameterSet& p;
	BoxSet boxset;
	double value_check[5];
	Interaction *interaction;
	int num_particle; ///< number of particles
	std::set <Interaction*> *interaction_list;
	std::unordered_set <int> *interaction_partners;
	int a_flag; // what is this?
	double err0start; // what is this?
	double lx; //< box size
	double ly; //< box size
	double lz; //< box size
	double lx_half; //< half box size
	double ly_half; //< half box size
	double lz_half; //< half box size
	double delta_time_max;
	double delta_time_min;
	double delta_time;
	double delta_time0;
	double delta_frames;
	double volume_fraction;
	double time;
	double couple_diff_norm4;
	double couple_diff_norm5;
	int t_count;
	double total_time;
	int total_iteration;
	double couple_diff_norm;
	int couple_iteration;
	bool twodimension;
	double force_result;
	std::vector<vec3d> particle_position_t;
	std::vector<vec3d> particle_position_t_p_dt;
	std::vector<double> radius;
	std::vector<vec3d> particle_velocity_t;
	
	int test_particle;
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	int periodize(vec3d&);
	void periodize_diff(vec3d&);
	void periodize_int(intPosition&);
	void setupSystem();
	void update();
	void assigneMaterialProperties();
	void setInitialCondition();
	void adaptiveTimestep();
	void reductionTimestep();
	void determineTimestep();
	void enlargeTimestep();
	void applyFlowBoundaryCondition();
	void internalCouplingInteractions();
	void setConfiguration(const std::vector <vec3d>& initial_positions,
						  const std::vector <double>& radii,
						  double lx_, double ly_, double lz_);
	void checkNewInteraction(std::vector<vec3d> &particle_position);
	void createNewInteraction(int i, int j, double scaled_interaction_range);
	void outputParticle(std::ofstream &fout);
	void outputFlow(std::ofstream &fout);
	void initializeBoxing();
};

#endif /* System_hpp */
