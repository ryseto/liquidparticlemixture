//
//  Simulation.hpp
//
//  Created by Ryohei Seto on 3/17/16.
//  Copyright © 2016 T. I. ZOHDI, Ryohei Seto. All rights reserved.
//

#ifndef Simulation_hpp
#define Simulation_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <sstream>
#include <ctime>
#include <map>
#include <algorithm>
#include "Global.h"
#include "System.hpp"
#include "Events.h"

class Simulation {
private:
	System sys;
	std::string indent;
	std::string header_imported_configulation[2];
	double time_end;
	double scale_factor;
	bool kill;
	std::ofstream fout_particle;
	std::ofstream fout_data;
	std::ofstream fout_flow;
	std::ofstream fout_rho;
	
	bool keepRunning();
	std::map<std::string,std::string> getConfMetaData(const std::string &,const std::string &);
	std::string getMetaParameter(std::map<std::string,std::string> &, std::string &, const std::string &);
	std::string getMetaParameter(std::map<std::string,std::string> &, std::string &);
	void outputYaplot();
public:
	Simulation();
	~Simulation();
	////////////////////////////////
	ParameterSet p;
	/*********** Events  ************/
	std::list <Event> events;
	////////////////////////////////
	void simulationSet1(std::string in_args,
						std::vector<std::string>& input_files);
	void setupSimulation(std::string in_args,
						 std::vector<std::string>& input_files);
	void setDefaultParameters();
	void readParameterFile(const std::string& filename_parameters);
	void autoSetParameters(const std::string &keyword, const std::string &value);
	void importConfiguration(const std::string& filename_import_positions);
};

#endif /* Simulation_hpp */
