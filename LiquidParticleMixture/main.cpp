//
//  main.cpp
//  LiquidParticleMixture
//
//  Created by Ryohei Seto on 3/17/16.
//  Copyright © 2016 T. I. ZOHDI, Ryohei Seto. All rights reserved.
//

#include <iostream>
#include <getopt.h>
#include <string>
#include <stdexcept>
#include "Global.h"
#include "Simulation.hpp"
using namespace std;

int main(int argc, char **argv)
{
	cout << endl << "SimulationTemplate version " << GIT_VERSION << endl << endl;
	string usage = " Simulation\n $ SimulationTemplate [Parameter_File] [ParticleConfig_File]";
	
	string param_filename = "not_given";
	string config_filename = "not_given";
	
	const struct option longopts[] = {
		{"help",              no_argument,       0, 'h'},
		{0, 0, 0, 0},
	};
	
	int index;
	int c;
	while ((c = getopt_long(argc, argv, "h", longopts, &index)) != -1) {
		switch (c) {
			case 'h':
				cerr << usage << endl;
				exit(1);
			case '?':
				/* getopt already printed an error message. */
				break;
			default:
				abort ();
		}
	}
	
	ostringstream in_args;
	for (int i=0; i<argc; i++) {
		in_args << argv[i] << " ";
	}
	if (optind == argc-2) {
		param_filename = argv[optind++];
		config_filename = argv[optind++];
	} else {
		cerr << usage << endl;
		exit(1);
	}
	vector <string> input_files(2);
	input_files[0] = param_filename;
	input_files[1] = config_filename;
	Simulation simulation;
	try {
		simulation.simulationSet1(in_args.str(), input_files);
	} catch (runtime_error& e) {
		cerr << e.what() << endl;
		return 1;
	}
	
	return 0;
}











