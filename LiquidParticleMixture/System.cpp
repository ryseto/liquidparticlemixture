//
//  System.cpp
//  LiquidParticleMixture
//
//  Created by Ryohei Seto on 3/17/16.
//  Copyright © 2016 T. I. ZOHDI, Ryohei Seto. All rights reserved.
//
#include "System.hpp"
#include <stdexcept>
#include <cmath>
#define DELETE(x) if(x){delete [] x; x = NULL;}
using namespace std;

System::System(ParameterSet& ps, std::list <Event>& ev):
events(ev),
p(ps),
twodimension(false)
{
	
}

void System::setupSystem()
{
	// meshsize
	dx_mesh = lx/p.x_mesh;
	dy_mesh = ly/p.y_mesh;
	dz_mesh = lz/p.z_mesh;
	dx2[0] = 2*dx_mesh;
	dx2[1] = 2*dy_mesh;
	dx2[2] = 2*dz_mesh;
	// prepare interaction objects at the beginning
	maxnum_interaction = num_particle*20;
	interaction = new Interaction [maxnum_interaction];
	for (int k=0; k<maxnum_interaction; k++) {
		interaction[k].init(this);
		interaction[k].set_label(k);
	}
	interaction_list = new std::set <Interaction*> [num_particle];
	interaction_partners = new std::unordered_set <int> [num_particle];
	// memory allocation
	particle_mass.resize(num_particle);
	particle_area.resize(num_particle);
	particle_velocity_t.resize(num_particle);
	particle_velocity_t_p_dt.resize(num_particle);
	particle_velocity_couple.resize(num_particle);
	force_t.resize(num_particle);
	force_t_p_dt.resize(num_particle);
	contact_force_t.resize(num_particle);
	contact_force_t_p_dt.resize(num_particle);
	ipos_t.resize(num_particle);
	ipos_t_p_dt.resize(num_particle);
	// 3D field
	int num_elements = p.x_mesh*p.y_mesh*p.z_mesh;
	size_array_double = sizeof(double)*num_elements;
	size_array_vec3d = sizeof(vec3d)*num_elements;
	size_array_StressTensor = sizeof(StressTensor)*num_elements;
	allocate3Dbox_vec3d(flow_velocity_t, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_vec3d(flow_velocity_t_p_dt, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_vec3d(flow_velocity_couple, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_vec3d(gridforces_t, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_vec3d(gridforces_t_p_dt, p.x_mesh, p.y_mesh, p.z_mesh);
	if (p.uniform_rho == false) {
		allocate3Dbox_double(rho, p.x_mesh, p.y_mesh, p.z_mesh);
		allocate3Dbox_double(rho_t_p_dt, p.x_mesh, p.y_mesh, p.z_mesh);
		allocate3Dbox_double(rho_t, p.x_mesh, p.y_mesh, p.z_mesh);
		allocate3Dbox_double(rho_initial, p.x_mesh, p.y_mesh, p.z_mesh);
		allocate3Dbox_vec3d(grad_rho_t_p_dt, p.x_mesh, p.y_mesh, p.z_mesh);
		allocate3Dbox_vec3d(grad_rho_t, p.x_mesh, p.y_mesh, p.z_mesh);
	}
	if (p.uniform_mu == false) {
		allocate3Dbox_double(mu, p.x_mesh, p.y_mesh, p.z_mesh);
		allocate3Dbox_double(mu_t_p_dt, p.x_mesh, p.y_mesh, p.z_mesh);
		allocate3Dbox_double(mu_t, p.x_mesh, p.y_mesh, p.z_mesh);
		allocate3Dbox_double(mu_initial, p.x_mesh, p.y_mesh, p.z_mesh);
	}
	allocate3Dbox_double(qf, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_double(qf_initial, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_double(pressure_t_p_dt, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_double(pressure_t, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_double(lambda_t_p_dt, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_double(lambda_t, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_stress(stress_t_p_dt, p.x_mesh, p.y_mesh, p.z_mesh);
	allocate3Dbox_stress(sym_velocity_grad_t_p_dt, p.x_mesh, p.y_mesh, p.z_mesh);
	rho_face = (1-volume_fraction)*p.rho_liquid+volume_fraction*p.rho2;
	rho_face_list.resize(6);
	velocity_face_list.resize(6);
	for (auto &rlist : rho_face_list) {
		rlist = rho_face;
	}
	velocity_face_list[0].set(p.outflow_velocity, 0, 0); // outflow
	velocity_face_list[1].set(p.inflow_velocity, 0, 0); // sideflow
	velocity_face_list[2].set(p.inflow_velocity, 0, 0); // inflow
	velocity_face_list[3].set(p.inflow_velocity, 0, 0); // sideflow
	velocity_face_list[4].set(p.inflow_velocity, 0, 0); // topflow
	velocity_face_list[5].set(p.inflow_velocity, 0, 0); // bottomflow
	const_ts1 = 1.0/(p.expo*(p.couple_iteration_limit-1));
}

void System::assigneMaterialProperties()
{
	double rho_value = (1-volume_fraction)*p.rho_liquid+volume_fraction*p.rho2;
	double mu_value  = (1-volume_fraction)*p.mu_liquid +volume_fraction*p.mu2;
	double qf_value  = (1-volume_fraction)*p.q_liquid  +volume_fraction*p.q2;
	rho_const = rho_value;
	mu_const = mu_value;
	cerr << "rho_value = " << rho_value << endl;
	cerr << "qf_value = "  << qf_value  << endl;
	cerr << "mu_value = "  << mu_value  << endl;
	for (int ix=0; ix<p.x_mesh; ix++) {
		for (int iy=0; iy<p.y_mesh; iy++) {
			for (int iz=0; iz<p.z_mesh; iz++) {
				if (p.uniform_rho == false) {
					rho_initial[ix][iy][iz] = rho_value;
					rho[ix][iy][iz]         = rho_value;
				}
				if (p.uniform_mu == false) {
					mu_initial[ix][iy][iz]  = mu_value;
					mu[ix][iy][iz]          = mu_value;
				}
				qf_initial[ix][iy][iz]  = qf_value;
				qf[ix][iy][iz]          = qf_value;
			}
		}
	}
}

void System::setInitialCondition()
{
	time = 0;
	total_iteration = 0;
	t_count = 0;
	vec3d pos;
	for (int ix=0; ix<p.x_mesh; ix++) {
		pos.x = ix*dx_mesh;
		for (int iy=0; iy<p.y_mesh; iy++) {
			pos.y = iy*dy_mesh;
			for (int iz=0; iz<p.z_mesh; iz++) {
				pos.z = iz*dz_mesh;
				double initial_velocity_x;
				if (p.off == 0) {
					initial_velocity_x = p.inflow_velocity;
				} else {
					double yprofile = pow(abs((pos.y-ly_half)/ly_half), p.profile);
					double zprofile = pow(abs((pos.z-lz_half)/lz_half), p.profile);
					initial_velocity_x = p.inflow_velocity*(1-p.off*yprofile)*(1-p.off*zprofile);
				}
				flow_velocity_t[ix][iy][iz].set(initial_velocity_x, 0, 0);
				flow_velocity_t_p_dt[ix][iy][iz].set(initial_velocity_x, 0, 0);
				if (p.uniform_rho == false) {
					rho_t_p_dt[ix][iy][iz] = rho[ix][iy][iz];
					rho_t[ix][iy][iz] = rho[ix][iy][iz];
				}
				if (p.uniform_mu == false) {
					mu_t_p_dt[ix][iy][iz] =  mu[ix][iy][iz];
					mu_t[ix][iy][iz] =  mu[ix][iy][iz];
					lambda_t_p_dt[ix][iy][iz] = (-2.0/3)*mu_t_p_dt[ix][iy][iz];
					lambda_t[ix][iy][iz]      = (-2.0/3)*mu_t[ix][iy][iz];
				} else {
					lambda_t_p_dt[ix][iy][iz] = (-2.0/3)*mu_const;
					lambda_t[ix][iy][iz]      = (-2.0/3)*mu_const;
				}
				pressure_t_p_dt[ix][iy][iz] = p.pressure0;
				pressure_t[ix][iy][iz]      = p.pressure0;
			}
		}
	}
	for (int ix=1; ix<p.x_mesh-1; ix++) {
		for (int iy=1; iy<p.y_mesh-1; iy++) {
			for (int iz=1; iz<p.z_mesh-1; iz++) {
				gridforces_t[ix][iy][iz].reset();
				gridforces_t_p_dt[ix][iy][iz].reset();
			}
		}
	}
	// FOR THE PARTICLES
	for (int i=0; i<num_particle; i++) {
		force_t[i].reset();
		force_t_p_dt[i].reset();
		particle_mass[i] = (4*M_PI/3)*p.rho_particle*radius[i]*radius[i]*radius[i];
		particle_area[i] = M_PI*radius[i]*radius[i];
	}
	if (p.motionless) {
		for (int i=0; i<num_particle; i++) {
			particle_velocity_t[i].reset();
			particle_velocity_t_p_dt[i].reset();
		}
	} else {
		if (p.off == 0) {
			for (int i=0; i<num_particle; i++) {
				particle_velocity_t[i].set(p.inflow_velocity, 0, 0);
				particle_velocity_t_p_dt[i].set(p.inflow_velocity, 0, 0);
			}
		} else {
			for (int i=0; i<num_particle; i++) {
				double yprofile = pow(abs((particle_position_t[i].y-ly_half)/ly_half), p.profile);
				double zprofile = pow(abs((particle_position_t[i].z-lz_half)/lz_half), p.profile);
				double velocity_x = p.inflow_velocity*(1-p.off*yprofile)*(1-p.off*zprofile);
				particle_velocity_t[i].set(velocity_x, 0, 0);
				particle_velocity_t_p_dt[i].set(velocity_x, 0, 0);
			}
		}
	}
	
	//@@@@@@@@@@@@@@@
	for (int i=0; i<5; i++) {
		value_check[i] = 0;
	}
	drag_coeff_search = 2*mu_const*6*M_PI;
}

void System::applyFlowBoundaryCondition()
{
	/*
	 * FACE 1: BOUNDARY CONDITIONS-OUTFLOW-X1 DIRECTION
	 * y-z plane at x = Lx
	 *
	 * and
	 *
	 * FACE 3: BOUNDARY CONDITIONS-INFLOW-X1 DIRECTION
	 * y-z plane at x = 0
	 */
	int ix_out = p.x_mesh-1;
	int ix_in = 0;
	vec3d pos;
	pos.x = 0;
	for (int iy=0; iy<p.y_mesh; iy++){
		pos.y = iy*p.y_mesh;
		for (int iz=0; iz<p.z_mesh; iz++){
			pos.z = iz*p.z_mesh;
			if (p.off == 0) {
				// Face 1
				flow_velocity_t[ix_out][iy][iz] = velocity_face_list[0];
				flow_velocity_t_p_dt[ix_out][iy][iz] = velocity_face_list[0];
				// Face 3
				flow_velocity_t[ix_in][iy][iz] = velocity_face_list[2];
				flow_velocity_t_p_dt[ix_in][iy][iz] = velocity_face_list[2];
			} else {
				cerr << "not implemented yet" << endl;
				exit(1);
				double outflow_vel = velocity_face_list[0].x;
				double inflow_vel = velocity_face_list[2].x;
				double yprofile = pow(abs((pos.y-ly_half)/ly_half), p.profile);
				double zprofile = pow(abs((pos.z-lz_half)/lz_half), p.profile);
				outflow_vel *= (1-p.off*yprofile)*(1-p.off*zprofile);
				inflow_vel  *= (1-p.off*yprofile)*(1-p.off*zprofile);
				flow_velocity_t[ix_out][iy][iz].set(outflow_vel, 0, 0);
				flow_velocity_t_p_dt[ix_out][iy][iz].set(outflow_vel, 0, 0);
				flow_velocity_t[ix_in][iy][iz].set(inflow_vel, 0, 0);
				flow_velocity_t_p_dt[ix_in][iy][iz].set(inflow_vel, 0, 0);
			}
			// Face 1
			if (p.uniform_rho == false){
				rho_t[ix_out][iy][iz]      = rho_face_list[0];
				rho_t_p_dt[ix_out][iy][iz] = rho_face_list[0];
				// Face 3
				rho_t[ix_in][iy][iz]      = rho_face_list[2];
				rho_t_p_dt[ix_in][iy][iz] = rho_face_list[2];
			}
		}
	}
	/*
	 * FACE 2: BOUNDARY CONDITIONS-SIDE WALL
	 * x-z plane at y = Ly
	 *
	 * and
	 *
	 * FACE 4: BOUNDARY CONDITIONS-SIDEWALL
	 * x-z plane at y = 0
	 */
	int iy_1 = p.y_mesh-1;
	int iy_2 = 0;
	for (int ix = 0; ix < p.x_mesh; ix++){
		for (int iz = 0; iz < p.z_mesh; iz++){
			// Face 2
			flow_velocity_t[ix][iy_1][iz]      = velocity_face_list[1];
			flow_velocity_t_p_dt[ix][iy_1][iz] = velocity_face_list[1];
			// Face 4
			flow_velocity_t[ix][iy_2][iz]      = velocity_face_list[3];
			flow_velocity_t_p_dt[ix][iy_2][iz] = velocity_face_list[3];
			if (p.uniform_rho == false) {
				rho_t[ix][iy_1][iz]      = rho_face_list[1];
				rho_t_p_dt[ix][iy_1][iz] = rho_face_list[1];
				rho_t[ix][iy_2][iz]      = rho_face_list[3];
				rho_t_p_dt[ix][iy_2][iz] = rho_face_list[3];
			}
		}
	}
	/*
	 * FACE 5: BOUNDARY CONDITIONS (TOP)
	 * x-y plane at z = Lz
	 *
	 * FACE 6: BOUNDARY CONDITIONS (BOTTOM)
	 * x-y plane at z = 0
	 */
	int iz_1 = p.z_mesh-1;
	int iz_2 = 0;
	for (int ix = 0; ix < p.x_mesh; ix++){
		for (int iy = 0; iy < p.y_mesh; iy++){
			// Face 5
			flow_velocity_t[ix][iy][iz_1]      = velocity_face_list[4];
			flow_velocity_t_p_dt[ix][iy][iz_1] = velocity_face_list[4];
			// Face 6
			flow_velocity_t[ix][iy][iz_2]      = velocity_face_list[5];
			flow_velocity_t_p_dt[ix][iy][iz_2] = velocity_face_list[5];
			if (p.uniform_rho == false) {
				rho_t[ix][iy][iz_1]      = rho_face_list[4];
				rho_t_p_dt[ix][iy][iz_1] = rho_face_list[4];
				rho_t[ix][iy][iz_2]      = rho_face_list[5];
				rho_t_p_dt[ix][iy][iz_2] = rho_face_list[5];
			}
		}
	}
}

void System::setCoupleVelocities()
{
	// TO MEASURE THE COUPLING
	memcpy(flow_velocity_couple[0][0], flow_velocity_t_p_dt[0][0], size_array_vec3d);
	for (int i=0; i<num_particle; i++) {
		particle_velocity_couple[i] = particle_velocity_t_p_dt[i];
	}
}

void System::calc_diff_mu(vec3d *diff_mu, // output
						  double *cross_diff_mu, // output
						  double ***mu_,
						  vec3d ***flow_velocity_,
						  int ix, int iy, int iz)
{
	if (p.uniform_mu == false) {
		// diff_mu[0] = (DMUDV1DX1DX1TPDT, DMUDV2DX1DX1TPDT, DMUDV3DX1DX1TPDT)
		diff_mu[0] = (1/dx_mesh)*( 0.5*(mu_[ix+1][iy][iz]+mu_[ix][iy][iz])  *(flow_velocity_[ix+1][iy][iz]-flow_velocity_[ix][iy][iz]  )/dx_mesh
								  -0.5*(mu_[ix][iy][iz]  +mu_[ix-1][iy][iz])*(flow_velocity_[ix][iy][iz]  -flow_velocity_[ix-1][iy][iz])/dx_mesh);
		// diff_mu[1] = (DMUDV1DX2DX2TPDT, DMUDV2DX2DX2TPDT, DMUDV3DX2DX2TPDT)
		diff_mu[1] = (1/dy_mesh)*( 0.5*(mu_[ix][iy+1][iz]+mu_[ix][iy][iz])  *(flow_velocity_[ix][iy+1][iz]-flow_velocity_[ix][iy][iz]  )/dy_mesh
								  -0.5*(mu_[ix][iy][iz]  +mu_[ix][iy-1][iz])*(flow_velocity_[ix][iy][iz]  -flow_velocity_[ix][iy-1][iz])/dy_mesh);
		// diff_mu[2] = (DMUDV1DX3DX3TPDT, DMUDV2DX3DX3TPDT, DMUDV3DX3DX3TPDT)
		diff_mu[2] = (1/dz_mesh)*( 0.5*(mu_[ix][iy][iz+1]+mu_[ix][iy][iz])  *(flow_velocity_[ix][iy][iz+1]-flow_velocity_[ix][iy][iz]  )/dz_mesh
								  -0.5*(mu_[ix][iy][iz]  +mu_[ix][iy][iz-1])*(flow_velocity_[ix][iy][iz]  -flow_velocity_[ix][iy][iz-1])/dz_mesh);
		//DMUDV1DX2DX1TPDT
		cross_diff_mu[0] =
		(1/(4*dx_mesh*dy_mesh))*( mu_[ix+1][iy][iz]*(flow_velocity_[ix+1][iy+1][iz].x-flow_velocity_[ix+1][iy-1][iz].x)
								 -mu_[ix-1][iy][iz]*(flow_velocity_[ix-1][iy+1][iz].x-flow_velocity_[ix-1][iy-1][iz].x));
		//DMUDV2DX1DX2TPDT
		cross_diff_mu[1] =
		(1/(4*dx_mesh*dy_mesh))*( mu_[ix][iy+1][iz]*(flow_velocity_[ix+1][iy+1][iz].y-flow_velocity_[ix-1][iy+1][iz].y)
								 -mu_[ix][iy-1][iz]*(flow_velocity_[ix+1][iy-1][iz].y-flow_velocity_[ix-1][iy-1][iz].y));
		//DMUDV2DX3DX2TPDT
		cross_diff_mu[2] =
		(1/(4*dy_mesh*dz_mesh))*( mu_[ix][iy+1][iz]*(flow_velocity_[ix][iy+1][iz+1].y-flow_velocity_[ix][iy+1][iz-1].y)
								 -mu_[ix][iy-1][iz]*(flow_velocity_[ix][iy-1][iz+1].y-flow_velocity_[ix][iy-1][iz-1].y));
		//DMUDV3DX2DX3TPDT
		cross_diff_mu[3] =
		(1/(4*dy_mesh*dz_mesh))*( mu_[ix][iy][iz+1]*(flow_velocity_[ix][iy+1][iz+1].z-flow_velocity_[ix][iy-1][iz+1].z)
								 -mu_[ix][iy][iz-1]*(flow_velocity_[ix][iy+1][iz-1].z-flow_velocity_[ix][iy-1][iz-1].z));
		//DMUDV3DX1DX3TPDT
		cross_diff_mu[4] =
		(1/(4*dz_mesh*dx_mesh))*( mu_[ix][iy][iz+1]*(flow_velocity_[ix+1][iy][iz+1].z-flow_velocity_[ix-1][iy][iz+1].z)
								 -mu_[ix][iy][iz-1]*(flow_velocity_[ix+1][iy][iz-1].z-flow_velocity_[ix-1][iy][iz-1].z));
		//DMUDV1DX3DX1TPDT
		cross_diff_mu[5] =
		(1/(4*dz_mesh*dx_mesh))*( mu_[ix+1][iy][iz]*(flow_velocity_[ix+1][iy][iz+1].x-flow_velocity_[ix+1][iy][iz-1].x)
								 -mu_[ix-1][iy][iz]*(flow_velocity_[ix-1][iy][iz+1].x-flow_velocity_[ix-1][iy][iz-1].x));
		
	} else {
		// diff_mu[0] = (DMUDV1DX1DX1TPDT, DMUDV2DX1DX1TPDT, DMUDV3DX1DX1TPDT)
		diff_mu[0] = (mu_const/(dx_mesh*dx_mesh))*(flow_velocity_[ix+1][iy][iz]-2*flow_velocity_[ix][iy][iz]+flow_velocity_[ix-1][iy][iz]);
		// diff_mu[1] = (DMUDV1DX2DX2TPDT, DMUDV2DX2DX2TPDT, DMUDV3DX2DX2TPDT)
		diff_mu[1] = (mu_const/(dy_mesh*dy_mesh))*(flow_velocity_[ix][iy+1][iz]-2*flow_velocity_[ix][iy][iz]+flow_velocity_[ix][iy-1][iz]);
		// diff_mu[2] = (DMUDV1DX3DX3TPDT, DMUDV2DX3DX3TPDT, DMUDV3DX3DX3TPDT)
		diff_mu[2] = (mu_const/(dz_mesh*dz_mesh))*(flow_velocity_[ix][iy][iz+1]-2*flow_velocity_[ix][iy][iz]+flow_velocity_[ix][iy][iz-1]);
		//DMUDV1DX2DX1TPDT
		cross_diff_mu[0] =(mu_const/(4*dx_mesh*dy_mesh))*(flow_velocity_[ix+1][iy+1][iz].x-flow_velocity_[ix+1][iy-1][iz].x
														  -flow_velocity_[ix-1][iy+1][iz].x+flow_velocity_[ix-1][iy-1][iz].x);
		//DMUDV2DX1DX2TPDT
		cross_diff_mu[1] =(mu_const/(4*dx_mesh*dy_mesh))*(flow_velocity_[ix+1][iy+1][iz].y-flow_velocity_[ix-1][iy+1][iz].y
														  -flow_velocity_[ix+1][iy-1][iz].y+flow_velocity_[ix-1][iy-1][iz].y);
		//DMUDV2DX3DX2TPDT
		cross_diff_mu[2] =(mu_const/(4*dy_mesh*dz_mesh))*(flow_velocity_[ix][iy+1][iz+1].y-flow_velocity_[ix][iy+1][iz-1].y
														  -flow_velocity_[ix][iy-1][iz+1].y+flow_velocity_[ix][iy-1][iz-1].y);
		//DMUDV3DX2DX3TPDT
		cross_diff_mu[3] =(mu_const/(4*dy_mesh*dz_mesh))*(flow_velocity_[ix][iy+1][iz+1].z-flow_velocity_[ix][iy-1][iz+1].z
														  -flow_velocity_[ix][iy+1][iz-1].z+flow_velocity_[ix][iy-1][iz-1].z);
		//DMUDV3DX1DX3TPDT
		cross_diff_mu[4] =(mu_const/(4*dz_mesh*dx_mesh))*(flow_velocity_[ix+1][iy][iz+1].z-flow_velocity_[ix-1][iy][iz+1].z
														  -flow_velocity_[ix+1][iy][iz-1].z+flow_velocity_[ix-1][iy][iz-1].z);
		//DMUDV1DX3DX1TPDT
		cross_diff_mu[5] =(mu_const/(4*dz_mesh*dx_mesh))*(flow_velocity_[ix+1][iy][iz+1].x-flow_velocity_[ix+1][iy][iz-1].x
														  -flow_velocity_[ix-1][iy][iz+1].x+flow_velocity_[ix-1][iy][iz-1].x);
	}
}

void System::calcViscosityField(vec3d *diff_mu_t_p_dt,
								vec3d *diff_mu_t,
								double *cross_diff_mu_t_p_dt,
								double *cross_diff_mu_t,
								int ix, int iy, int iz)
{
	vec3d diff_lambda_t_p_dt[3];
	vec3d diff_lambda_t[3];
	//@@@ ????
	if (p.mu_sens != 0) {
		mu_t_p_dt[ix][iy][iz] = mu_initial[ix][iy][iz]*(1+p.mu_sens*rho_t_p_dt[ix][iy][iz]/rho_initial[ix][iy][iz]);
		lambda_t_p_dt[ix][iy][iz] = -(2.0/3)*mu_t_p_dt[ix][iy][iz];
	} else {
		/*
		 * mu[ix][iy][iz] are constant during entire simulation
		 */
		//mu_t_p_dt[ix][iy][iz] = mu_initial[ix][iy][iz];
	}
	// THE ACTUAL DERIVATIVES AT TPDT
	if (p.imcompressible) {
		// @@@@ This is not neccesary
		diff_lambda_t_p_dt[0].set(0, 0, 0);
		diff_lambda_t_p_dt[1].set(0, 0, 0);
		diff_lambda_t_p_dt[2].set(0, 0, 0);
	} else {
		std::cerr << "compressible case is not implemented yet.\n";
		//  DLAMDV1DX1DX1TPDT=(1.0D0/DELTAX1)*( .....
		exit(1);
	}
	calc_diff_mu(diff_mu_t_p_dt, cross_diff_mu_t_p_dt, mu_t_p_dt, flow_velocity_t_p_dt, ix, iy, iz);
	
	// THE ACTUAL DERIVATIVES AT T
	if (p.imcompressible) {
		diff_lambda_t[0].set(0, 0, 0);
		diff_lambda_t[1].set(0, 0, 0);
		diff_lambda_t[2].set(0, 0, 0);
	} else {
		std::cerr << "compressible case is not implemented yet.\n";
		exit(1);
	}
	calc_diff_mu(diff_mu_t, cross_diff_mu_t, mu_t, flow_velocity_t, ix, iy, iz);
}

void System::mesh_gradient(vec3d& gradient, int ix, int iy, int iz, double ***scalar_field)
{
	//	int iy_p_1 = iy+1;
	//	if (iy_p_1 == p.y_mesh) {
	//		iy_p_1 = 0;
	//	}
	//	int iy_m_1 = iy-1;
	//	if (iy_m_1 == -1) {
	//		iy_m_1 = p.y_mesh-1;
	//	}
	//	int iz_p_1 = iz+1;
	//	if (iz_p_1 == p.z_mesh) {
	//		iz_p_1 = 0;
	//	}
	//	int iz_m_1 = iz-1;
	//	if (iz_m_1 == -1) {
	//		iz_m_1 = p.z_mesh-1;
	//	}
	
	//	DRHODX1TPDT(I,J,K)=(RHOTPDT(I+1,J,K)-RHOTPDT(I-1,J,K))/(2.0D0*DELTAX1)
	gradient.x = (scalar_field[ix+1][iy][iz]-scalar_field[ix-1][iy][iz])/dx2[0];
	gradient.y = (scalar_field[ix][iy+1][iz]-scalar_field[ix][iy-1][iz])/dx2[1];
	gradient.z = (scalar_field[ix][iy][iz+1]-scalar_field[ix][iy][iz-1])/dx2[2];
}

void System::calcGradients(vec3d *diff_velocity_t_p_dt, vec3d *diff_velocity_t,
						   vec3d &convective_t_p_dt, vec3d &convective_t,
						   int ix, int iy, int iz)
{
	// GRADIENTS AT TIME=T+DELTAT
	// diff_velocity_t_p_dt[0] = (DV1DX1TPDT, DV2DX1TPDT, DV3DX1TPDT)
	// diff_velocity_t_p_dt[1] = (DV1DX2TPDT, DV2DX2TPDT, DV3DX2TPDT)
	// diff_velocity_t_p_dt[2] = (DV1DX3TPDT, DV2DX3TPDT, DV3DX3TPDT)
	diff_velocity_t_p_dt[0] = (flow_velocity_t_p_dt[ix+1][iy][iz]-flow_velocity_t_p_dt[ix-1][iy][iz])/dx2[0];
	diff_velocity_t_p_dt[1] = (flow_velocity_t_p_dt[ix][iy+1][iz]-flow_velocity_t_p_dt[ix][iy-1][iz])/dx2[1];
	diff_velocity_t_p_dt[2] = (flow_velocity_t_p_dt[ix][iy][iz+1]-flow_velocity_t_p_dt[ix][iy][iz-1])/dx2[2];
	// GRADIENTS AT TIME=T
	diff_velocity_t[0]      = (flow_velocity_t[ix+1][iy][iz]-flow_velocity_t[ix-1][iy][iz])/dx2[0];
	diff_velocity_t[1]      = (flow_velocity_t[ix][iy+1][iz]-flow_velocity_t[ix][iy-1][iz])/dx2[1];
	diff_velocity_t[2]      = (flow_velocity_t[ix][iy][iz+1]-flow_velocity_t[ix][iy][iz-1])/dx2[2];
	// FOR FLVID MECHANICS OPTION <--- Fluid?
	// diff_velocity_t_p_dt[0] = (DV1DX1TPDT, DV2DX1TPDT, DV3DX1TPDT)
	// diff_velocity_t_p_dt[1] = (DV1DX2TPDT, DV2DX2TPDT, DV3DX2TPDT)
	// diff_velocity_t_p_dt[2] = (DV1DX3TPDT, DV2DX3TPDT, DV3DX3TPDT)
	// AT TIME=T+DELTAT
	// CONVECTIVEX1TPDT=DV1DX1TPDT*V1TPDT(I,J,K)+DV1DX2TPDT*V2TPDT(I,J,K)+DV1DX3TPDT*V3TPDT(I,J,K)
	// CONVECTIVEX1TPDT=diff_velocity_t_p_dt[0].x*flow_velocity_t_p_dt.x +diff_velocity_t_p_dt[1].x*flow_velocity_t_p_dt.y +diff_velocity_t_p_dt.x*flow_velocity_t_p_dt.z
	// CONVECTIVEX2TPDT=diff_velocity_t_p_dt[0].y*flow_velocity_t_p_dt.x +diff_velocity_t_p_dt[1].y*flow_velocity_t_p_dt.y +diff_velocity_t_p_dt.y*flow_velocity_t_p_dt.z
	// CONVECTIVEX3TPDT=diff_velocity_t_p_dt[0].z*flow_velocity_t_p_dt.x +diff_velocity_t_p_dt[1].z*flow_velocity_t_p_dt.y +diff_velocity_t_p_dt.z*flow_velocity_t_p_dt.z
	convective_t_p_dt = diff_velocity_t_p_dt[0]*flow_velocity_t_p_dt[ix][iy][iz].x
	+diff_velocity_t_p_dt[1]*flow_velocity_t_p_dt[ix][iy][iz].y
	+diff_velocity_t_p_dt[2]*flow_velocity_t_p_dt[ix][iy][iz].z;
	// AT TIME=T
	convective_t = diff_velocity_t[0]*flow_velocity_t[ix][iy][iz].x
	+diff_velocity_t[1]*flow_velocity_t[ix][iy][iz].y
	+diff_velocity_t[2]*flow_velocity_t[ix][iy][iz].z;
	/**************************************************************************
	 THE MIXED MATERIAL PROPERTIES AT TPDT-FOR AN EULERIAN GRID
	 
	 NOTE THAT EVEN THOUGH THE MATERIALS ARE ASSUMED INCOMPRESSIBLE
	 THEY MIX AND WE HAVE TO KEEP TRACK OF THE MATERIAL
	 THIS IS EASY TO DO WITHE A LAGRANGIAN APPROACH-THE RESPECTIVE
	 DENSITIES DO NOT CHANGE AND WE FOLLOW THE MATERIAL-AND DIFFICULT
	 TO DO WITH AND EULERIAN APPROACH-AND THERE IS ALSO MIXING-FOR
	 THIS WE NEED THE CONTINUITY EQUATION WITH div v=0 (J=1) ENFORCED
	 ***************************************************************************/
	if (p.uniform_rho == false) {
		// TPDT GRAD RHO
		mesh_gradient(grad_rho_t_p_dt[ix][iy][iz], ix, iy, iz, rho_t_p_dt);
		// T GRAD RHO
		mesh_gradient(grad_rho_t[ix][iy][iz], ix, iy, iz, rho_t);
	}
}

void System::derivativeStress(double *d_stress, vec3d *diff_velocity, vec3d *diff_mu, double *cross_diff_mu_)
{
	// diff_velocity_t_p_dt[0] = (DV1DX1TPDT, DV2DX1TPDT, DV3DX1TPDT)
	// diff_velocity_t_p_dt[1] = (DV1DX2TPDT, DV2DX2TPDT, DV3DX2TPDT)
	// diff_velocity_t_p_dt[2] = (DV1DX3TPDT, DV2DX3TPDT, DV3DX3TPDT)
	//DSTRESS11DX1TPDT=-DPDX1TPDT+DLAMDV1DX1DX1TPDT+DLAMDV2DX2DX1TPDT+DLAMDV3DX3DX1TPDT+DMUDV1DX1DX1TPDT+DMUDV1DX1DX1TPDT
	//DSTRESS11DX1TPDT=-diff_velocity_t_p_dt[0].x
	// In the incompressible case, DLAMDV1DX1DX1T = 0
	//	 DMUDV1DX2DX2TPDT
	// cross_diff_mu[4] = DMUDV3DX1DX3TPDT
	// cross_diff_mu[0] = DMUDV1DX2DX1TPDT //
	// cross_diff_mu[3] = DMUDV3DX2DX3TPDT
	// cross_diff_mu[5] = DMUDV1DX3DX1TPDT
	// cross_diff_mu[2] = DMUDV2DX3DX2TPDT
	// diff_mu[0] = (DMUDV1DX1DX1TPDT, DMUDV2DX1DX1TPDT, DMUDV3DX1DX1TPDT)
	// diff_mu[1] = (DMUDV1DX2DX2TPDT, DMUDV2DX2DX2TPDT, DMUDV3DX2DX2TPDT)
	// diff_mu[2] = (DMUDV1DX3DX3TPDT, DMUDV2DX3DX3TPDT, DMUDV3DX3DX3TPDT)
	d_stress[0] = -diff_velocity[0].x+2*diff_mu[0].x; // xx
	d_stress[1] =                       diff_mu[1].x+cross_diff_mu_[1]; // xy
	d_stress[2] =                       diff_mu[2].x+cross_diff_mu_[4]; // xz
	d_stress[3] =                       diff_mu[0].y+cross_diff_mu_[0]; // yx
	d_stress[4] = -diff_velocity[1].y+2*diff_mu[1].y; // yy
	d_stress[5] =                       diff_mu[2].y+cross_diff_mu_[3]; // yz
	d_stress[6] =                       diff_mu[0].z+cross_diff_mu_[5]; // zx
	d_stress[7] =                       diff_mu[1].z+cross_diff_mu_[2]; // zy
	d_stress[8] = -diff_velocity[2].z+2*diff_mu[2].z; // zz
}

void System::fieldCalculationPostprocess(int ix, int iy, int iz, vec3d* diff_velocity_t_p_dt)
{
	// POST-PROCESSING THE STRESS AT TPDT
	// [NOTE] the order of stress elements is (xx, xy, xz, yz, yy, zz)
	// xx
	stress_t_p_dt[ix][iy][iz].elm[0] = -pressure_t_p_dt[ix][iy][iz]+mu_t_p_dt[ix][iy][iz]*(diff_velocity_t_p_dt[0].x+diff_velocity_t_p_dt[0].x);
	// xy
	stress_t_p_dt[ix][iy][iz].elm[1] =                              mu_t_p_dt[ix][iy][iz]*(diff_velocity_t_p_dt[1].x+diff_velocity_t_p_dt[0].y);
	// xz
	stress_t_p_dt[ix][iy][iz].elm[2] =                              mu_t_p_dt[ix][iy][iz]*(diff_velocity_t_p_dt[2].x+diff_velocity_t_p_dt[0].z);
	// yz
	stress_t_p_dt[ix][iy][iz].elm[3] =                              mu_t_p_dt[ix][iy][iz]*(diff_velocity_t_p_dt[2].y+diff_velocity_t_p_dt[1].z);
	// yy
	stress_t_p_dt[ix][iy][iz].elm[4] = -pressure_t_p_dt[ix][iy][iz]+mu_t_p_dt[ix][iy][iz]*(diff_velocity_t_p_dt[1].y+diff_velocity_t_p_dt[1].y);
	// zz
	stress_t_p_dt[ix][iy][iz].elm[5] = -pressure_t_p_dt[ix][iy][iz]+mu_t_p_dt[ix][iy][iz]*(diff_velocity_t_p_dt[2].z+diff_velocity_t_p_dt[2].z);
	
	// [NOTE] the order of stress elements is (xx, xy, xz, yz, yy, zz)
	// xx
	sym_velocity_grad_t_p_dt[ix][iy][iz].elm[0] = diff_velocity_t_p_dt[0].x;
	// xy
	sym_velocity_grad_t_p_dt[ix][iy][iz].elm[1] = 0.5*(diff_velocity_t_p_dt[0].y+diff_velocity_t_p_dt[1].x);
	// xz
	sym_velocity_grad_t_p_dt[ix][iy][iz].elm[2] = 0.5*(diff_velocity_t_p_dt[0].z+diff_velocity_t_p_dt[2].x);
	// yz
	sym_velocity_grad_t_p_dt[ix][iy][iz].elm[3] = 0.5*(diff_velocity_t_p_dt[1].z+diff_velocity_t_p_dt[2].y);
	// yy
	sym_velocity_grad_t_p_dt[ix][iy][iz].elm[4] = diff_velocity_t_p_dt[1].y;
	// zz
	sym_velocity_grad_t_p_dt[ix][iy][iz].elm[5] = diff_velocity_t_p_dt[2].z;
}

void System::fieldCalculation(int ix, int iy, int iz)
{
	vec3d diff_velocity_t_p_dt[3];
	vec3d diff_velocity_t[3];
	vec3d convective_t_p_dt;
	vec3d convective_t;
	vec3d diff_mu_t_p_dt[3];
	vec3d diff_mu_t[3];
	diff_mu_t[0].reset();
	diff_mu_t[1].reset();
	diff_mu_t[2].reset();
	diff_mu_t_p_dt[0].reset();
	diff_mu_t_p_dt[1].reset();
	diff_mu_t_p_dt[2].reset();
	double cross_diff_mu_t_p_dt[6];
	double cross_diff_mu_t[6];
	vec3d grad_pressure_t_p_dt;
	vec3d grad_pressure_t;
	double d_stress_t_p_dt[9];
	double d_stress_t[9];
	// THE DERIVATIVES OF THE VELOCITY
	calcGradients(diff_velocity_t_p_dt, diff_velocity_t, convective_t_p_dt, convective_t, ix, iy, iz);
	// CONTINUITY
	// INCOMPRESSIBLE case
	// The COMPRESSIBLE case is implemented in the original code.
	
	// V1TPDT(I,J,K)*DRHODX1TPDT(I,J,K)+V2TPDT(I,J,K)*DRHODX2TPDT(I,J,K)+V3TPDT(I,J,K)*DRHODX3TPDT(I,J,K))+
	// flow_velocity_t_p_dt[ix][iy][iz].x*grad_rho_t_p_dt[ix][iy][iz].x + ....
	/// [Incompressible case but this is not effective]
	if (p.uniform_rho == false) {
		rho_t_p_dt[ix][iy][iz] = rho_t[ix][iy][iz]-delta_time*(p.phi*dot(flow_velocity_t_p_dt[ix][iy][iz], grad_rho_t_p_dt[ix][iy][iz])
															   +(1-p.phi)*dot(flow_velocity_t[ix][iy][iz], grad_rho_t[ix][iy][iz]));
	}
	//THE VISCOSITY RULE
	calcViscosityField(diff_mu_t_p_dt, diff_mu_t, cross_diff_mu_t_p_dt, cross_diff_mu_t, ix, iy, iz);
	
	// THE PRESSURE (ASSUMED CONSTANT-BUT YOU COULD ADD AN EQUATION OF STATE)
	pressure_t_p_dt[ix][iy][iz] = p.pressure0;
	// THE DERIVATIVES OF THE PRESSURE
	// (ASSUMED CONSTANT ABOVE-BUT YOU COULD ADD AN EQUATION OF STATE)
	// grad_pressure_t_p_dt is always zero?
	mesh_gradient(grad_pressure_t_p_dt, ix, iy, iz, pressure_t_p_dt);
	mesh_gradient(grad_pressure_t, ix, iy, iz, pressure_t);
	// THE DERIVATIVES OF THE STRESS
	// @@ diff_lambda_t is omitted.
	derivativeStress(d_stress_t_p_dt, diff_velocity_t_p_dt, diff_mu_t_p_dt, cross_diff_mu_t_p_dt);
	derivativeStress(d_stress_t,      diff_velocity_t,      diff_mu_t,      cross_diff_mu_t);
	// THE ELECTROMAGNETIC LOAD-AS A FUNCTION OF THE MIXTURE
	//  @@@ Omit from L1476 to L1589 in the test simulation
	//	vec3d em_load_t;
	//	vec3d em_load_t_p_dt;
	//	vec3d mag_load_t;
	//	vec3d mag_load_t_p_dt;
	updateFlowVelocity(d_stress_t_p_dt, d_stress_t, convective_t_p_dt, convective_t, ix, iy, iz);
	//fieldCalculationPostprocess(ix, iy, iz, diff_velocity_t_p_dt);
}

void System::updateFlowVelocity(double *d_stress_t_p_dt, double *d_stress_t,
								vec3d &convective_t_p_dt, vec3d &convective_t,
								int ix, int iy, int iz)
{
	// THE VELOCITY UPDATE
	//(DSTRESS11DX1TPDT+DSTRESS12DX2TPDT+DSTRESS13DX3TPDT+EMLOAD1TPDT+MAGLOAD1TPDT)
	vec3d tmp1_t_p_dt; // = em_load_t_p_dt+mag_load_t_p_dt;
	tmp1_t_p_dt.x += d_stress_t_p_dt[0]+d_stress_t_p_dt[1]+d_stress_t_p_dt[2];
	tmp1_t_p_dt.y += d_stress_t_p_dt[3]+d_stress_t_p_dt[4]+d_stress_t_p_dt[5];
	tmp1_t_p_dt.z += d_stress_t_p_dt[6]+d_stress_t_p_dt[7]+d_stress_t_p_dt[8];
	vec3d tmp1_t; // = em_load_t+mag_load_t;
	tmp1_t.x += d_stress_t[0]+d_stress_t[1]+d_stress_t[2];
	tmp1_t.y += d_stress_t[3]+d_stress_t[4]+d_stress_t[5];
	tmp1_t.z += d_stress_t[6]+d_stress_t[7]+d_stress_t[8];
	vec3d tmp2;
	if (p.uniform_rho == false) {
		tmp2 = (p.phi/rho_t_p_dt[ix][iy][iz])*tmp1_t_p_dt+((1-p.phi)/rho_t[ix][iy][iz])*tmp1_t;
	} else {
		tmp2 = (p.phi*tmp1_t_p_dt+(1-p.phi)*tmp1_t)/rho_const;
	}
	tmp2 -= p.phi*convective_t_p_dt+(1-p.phi)*convective_t;
	if (abs(convective_t_p_dt.x) > 0){
		vec3d tmp3 = p.phi*convective_t_p_dt+(1-p.phi)*convective_t;
		value_check[1] += tmp3.z;
	}
	
	if (p.damping != 0) {
		// damping = 0 in default
		tmp2 += p.damping*((p.phi/rho_t_p_dt[ix][iy][iz])*flow_velocity_t_p_dt[ix][iy][iz]
						   +((1-p.phi)/rho_t[ix][iy][iz])*flow_velocity_t[ix][iy][iz]);
		exit(1);
	}
	if (p.uniform_rho == false) {
		tmp2 += (p.phi/rho_t_p_dt[ix][iy][iz])*gridforces_t_p_dt[ix][iy][iz]+((1-p.phi)/rho_t[ix][iy][iz])*gridforces_t[ix][iy][iz];
	} else {
		tmp2 += (1/rho_const)*(p.phi*gridforces_t_p_dt[ix][iy][iz]+(1-p.phi)*gridforces_t[ix][iy][iz]);
		if (abs(gridforces_t_p_dt[ix][iy][iz].x) > 0){
			vec3d tmp3 = p.phi*gridforces_t_p_dt[ix][iy][iz]+(1-p.phi)*gridforces_t[ix][iy][iz];
			value_check[0] += tmp3.z;
		}
	}
	flow_velocity_t_p_dt[ix][iy][iz] = flow_velocity_t[ix][iy][iz]+delta_time*tmp2;
}


void System::trimingRange(intPosition ip_, int *ix_range, int *iy_range, int *iz_range)
{
	// x
	ix_range[0] = ip_.ix-pert[0];
	if (ix_range[0] < 0) {
		ix_range[0] = 0;
	}
	ix_range[1] = ip_.ix+pert[0];
	if (ix_range[1] >= p.x_mesh) {
		ix_range[1] = p.x_mesh-1;
	}
	// y
	iy_range[0] = ip_.iy-pert[1];
	if (iy_range[0] < 0) {
		iy_range[0] = 0;
	}
	iy_range[1] = ip_.iy+pert[1];
	if (iy_range[1] >= p.y_mesh) {
		iy_range[1] = p.y_mesh-1;
	}
	// z
	iz_range[0] = ip_.iz-pert[2];
	if (iz_range[0] < 0) {
		iz_range[0] = 0;
	}
	iz_range[1] = ip_.iz+pert[2];
	if (iz_range[1] >= p.z_mesh) {
		iz_range[1] = p.z_mesh-1;
	}
}

void System::ParticleFluidInteraction(int i, intPosition ip_,
									  vec3d &p_position_,
									  vec3d &p_velocity_,
									  vec3d ***flow_velocity_,
									  double ***rho_,
									  double ***mu_,
									  vec3d &force_,
									  vec3d ***&gridforces_)
{
	double volfac = dx_mesh*dy_mesh*dz_mesh;
	
	double rho_average = 0;
	double mu_average;
	
	if (p.uniform_mu == false) {
		mu_average = mu_[ip_.ix][ip_.iy][ip_.iz];
	} else {
		mu_average = mu_const;
	}
	double sq_radius11 = radius[i]*radius[i]*1.1*1.1;  // why 1.1 times larger?
	int ix_range[2];
	int iy_range[2];
	int iz_range[2];
	trimingRange(ip_, ix_range, iy_range, iz_range);
	vec3d pos;
	vec3d velocity_average;
	particle_domain.clear();
	for (int ix=ix_range[0]; ix<=ix_range[1]; ix++) {
		pos.x = ix*dx_mesh;
		for (int iy=iy_range[0]; iy<=iy_range[1]; iy++) {
			pos.y = iy*dy_mesh;
			for (int iz=iz_range[0]; iz<=iz_range[1]; iz++) {
				pos.z = iz*dz_mesh;
				double sq_distance = (pos-p_position_).sq_norm();
				if (sq_distance < sq_radius11) {
					intPosition ip = {ix, iy, iz};
					velocity_average += flow_velocity_[ip.ix][ip.iy][ip.iz];
					if (p.uniform_rho == false) {
						rho_average += rho_[ip.ix][ip.iy][ip.iz];
					}
					if (p.uniform_mu == false) {
						mu_average += mu_[ip.ix][ip.iy][ip.iz];
					}
					particle_domain.push_back(ip);
				}
			}
		}
	}
	int number_of_cells = (int)particle_domain.size();
	if (number_of_cells == 0) {
		p_position_.cerr();
		cerr << ix_range[0] << ' '<< ix_range[1] << endl;
		cerr << iy_range[0] << ' '<< iy_range[1] << endl;
		cerr << iz_range[0] << ' '<< iz_range[1] << endl;
		cerr << ip_.ix << ' ' << ip_.iy << ' ' << ip_.iz << endl;
		cerr << "it should not be here." << endl;
		exit(1);
		//		velocity_average = flow_velocity_[ipos_[i].ix][ipos_[i].iy][ipos_[i].iz];
		//		rho_average      =           rho_[ipos_[i].ix][ipos_[i].iy][ipos_[i].iz];
		//		mu_average       =            mu_[ipos_[i].ix][ipos_[i].iy][ipos_[i].iz];
	} else {
		velocity_average *= (1.0/number_of_cells);
		if (p.uniform_rho == false){
			rho_average *= (1.0/number_of_cells);
		} else {
			rho_average = rho_const;
		}
		//mu_average       *= (1.0/number_of_cells);
	}
	//  CDRAG IS INTRERPRETED AS DRAG PER FICTICIOUS VOLUME
	if (p.drag_method == 1) {
		double volume = (4.0/3)*M_PI*radius[i]*radius[i]*radius[i];
		force_ = p.coeff_drag*volume*(velocity_average-p_velocity_);
	} if (p.drag_method == 3) {
		double vnorm = (velocity_average-p_velocity_).norm();
		double drag_coeff = p.coeff_drag*0.5*vnorm*rho_average*particle_area[i];
		force_ = drag_coeff*(velocity_average-p_velocity_);
	} else if (p.drag_method == 4) {
		double stokesian = mu_average*6*M_PI*radius[i];
		force_ = stokesian*(velocity_average-p_velocity_);
	} else if (p.drag_method == 5) {
		//drag_coeff_search = 0.203973;
		force_ = drag_coeff_search*radius[i]*(velocity_average-p_velocity_);
	} else {
		std::cerr << "not implemented yet" << std::endl;
		exit(1);
	}
	if (number_of_cells == 0) {
		exit(1);
		//gridforces_[ipos_[i].ix][ipos_[i].iy][ipos_[i].iz] = -force_/(volfac);
	} else {
		//cerr << volfac << endl;
		for (const auto& ip : particle_domain) {
			gridforces_[ip.ix][ip.iy][ip.iz] = -force_/(number_of_cells*volfac);
		}
	}
}

inline int round_positive(double val)
{
	return (int)(val+0.5);
}

void System::periodize_int(intPosition& ipos_)
{
	if (ipos_.ix == -1) {
		ipos_.ix = 0;
	} else if (ipos_.ix >= p.x_mesh) {
		ipos_.ix = p.x_mesh-1;
	}
	if (ipos_.iy == -1) {
		ipos_.iy = 0;
	} else if (ipos_.iy == p.y_mesh) {
		ipos_.iy = p.y_mesh-1;
	}
	if (ipos_.iz == -1) {
		ipos_.iz = 0;
	} else if (ipos_.iz == p.z_mesh) {
		ipos_.iz = p.z_mesh-1;
	}
	//	if (ipos_.ix < 0) {
	//		ipos_.ix += p.x_mesh;
	//	} else if (ipos_.ix >= p.x_mesh) {
	//		ipos_.ix -= p.x_mesh;
	//	}
	//	if (ipos_.iy < 0) {
	//		ipos_.iy += p.y_mesh;
	//	} else if (ipos_.iy == p.y_mesh) {
	//		ipos_.iy -= p.y_mesh;
	//	}
	//	if (ipos_.iz < 0) {
	//		ipos_.iz += p.z_mesh;
	//	} else if (ipos_.iz == p.z_mesh) {
	//		ipos_.iz -= p.z_mesh;
	//	}
}

void System::dragForces()
{
	for (int ix=0; ix<p.x_mesh; ix++) {
		for (int iy=0; iy<p.y_mesh; iy++) {
			for (int iz=0; iz<p.z_mesh; iz++) {
				gridforces_t[ix][iy][iz].set(0, 0, 0);
				gridforces_t_p_dt[ix][iy][iz].set(0, 0, 0);
			}
		}
	}
	for (auto &f : force_t) {
		f.reset();
	}
	for (auto &f : force_t_p_dt) {
		f.reset();
	}
	for (int i=0; i<num_particle; i++) {
		ipos_t[i].ix = round_positive(particle_position_t[i].x/dx_mesh);
		ipos_t[i].iy = round_positive(particle_position_t[i].y/dy_mesh);
		ipos_t[i].iz = round_positive(particle_position_t[i].z/dz_mesh);
		//periodize_int(ipos_t[i]);
		ipos_t_p_dt[i].ix = round_positive(particle_position_t_p_dt[i].x/dx_mesh);
		ipos_t_p_dt[i].iy = round_positive(particle_position_t_p_dt[i].y/dy_mesh);
		ipos_t_p_dt[i].iz = round_positive(particle_position_t_p_dt[i].z/dz_mesh);
		//periodize_int(ipos_t_p_dt[i]);
		pert[0] = radius[i]/dx_mesh;
		pert[1] = radius[i]/dy_mesh;
		pert[2] = radius[i]/dz_mesh;
		// AT TIME T+DT
		ParticleFluidInteraction(i, ipos_t_p_dt[i], particle_position_t_p_dt[i], particle_velocity_t_p_dt[i],
								 flow_velocity_t_p_dt, rho_t_p_dt, mu_t_p_dt, force_t_p_dt[i], gridforces_t_p_dt);
		// AT TIME T
		ParticleFluidInteraction(i, ipos_t[i], particle_position_t[i], particle_velocity_t[i],
								 flow_velocity_t, rho_t, mu_t, force_t[i], gridforces_t);
	}
	
}

void System::updateInteractions()
{
	/**
	 \brief Updates the state of active interactions.
	 
	 To be called after particle moved.
	 Note that this routine does not look for new interactions (this is done by System::checkNewInteraction), it only updates already known active interactions.
	 It however desactivate interactions removes interactions that became inactive (ie when the distance between particles gets larger than the interaction range).
	 */
	for (int k=0; k<num_interaction; k++) {
		if (interaction[k].is_active()) {
			bool deactivated = false;
			interaction[k].updateState(deactivated);
			if (deactivated) {
				deactivated_interaction.push(k);
			}
		}
	}
}

void System::update()
{
	// copy 3D array from flow_velocity_t_p_dt to flow_velocity_t
	memcpy(flow_velocity_t[0][0], flow_velocity_t_p_dt[0][0], size_array_vec3d);
	if (p.uniform_mu == false) {
		memcpy(mu_t[0][0], mu_t_p_dt[0][0], size_array_double);
		memcpy(lambda_t[0][0], lambda_t_p_dt[0][0], size_array_double);
	}
	if (p.uniform_rho == false) {
		memcpy(rho_t[0][0], rho_t_p_dt[0][0], size_array_double);
	}
	memcpy(pressure_t[0][0], pressure_t_p_dt[0][0], size_array_double);
	for (int i=0; i<num_particle; i++) {
		particle_position_t[i] = particle_position_t_p_dt[i];
		periodize(particle_position_t[i]);
		boxset.box(i);
		particle_velocity_t[i] = particle_velocity_t_p_dt[i];
	}
	// @@ monitor
	//particle_position_t_p_dt[200].cerr();
	//particle_velocity_t_p_dt[200].cerr();
}

void System::updateVelocityPositionInIteration()
{
	for (auto &cf : contact_force_t) {
		cf.reset();
	}
	for (auto &cf : contact_force_t_p_dt) {
		cf.reset();
	}
	for (int k=0; k<num_interaction; k++) {
		if (interaction[k].is_active()) {
			unsigned int i1, i2;
			interaction[k].get_par_num(i1, i2);
			contact_force_t[i1] += interaction[k].contact_force_t;
			contact_force_t[i2] -= interaction[k].contact_force_t;
			contact_force_t_p_dt[i1] += interaction[k].contact_force_t_p_dt;
			contact_force_t_p_dt[i2] -= interaction[k].contact_force_t_p_dt;
		}
	}
	double coeff_stokesian = mu_const*6*M_PI*radius[0];
	vec3d force_ex(-coeff_stokesian*p.inflow_velocity, 0, 0);
	vec3d f_tmp, v_tmp;
	for (int i=0; i<num_particle; i++) {
		f_tmp = p.phi*(force_t_p_dt[i]+contact_force_t_p_dt[i])+(1-p.phi)*(force_t[i]+contact_force_t[i])+force_ex;
		particle_velocity_t_p_dt[i] = particle_velocity_t[i]+(delta_time/particle_mass[i])*f_tmp;
		v_tmp = p.phi*particle_velocity_t_p_dt[i]+(1-p.phi)*particle_velocity_t[i];
		particle_position_t_p_dt[i] = particle_position_t[i]+delta_time*v_tmp;
		periodize(particle_position_t_p_dt[i]);
	}
	drag_coeff_search -= delta_time*v_tmp.x;
}

void System::internalCouplingInteractions()
{
	while (couple_diff_norm > p.couple_tolerance || couple_iteration < 2) {
		couple_iteration ++;
		cerr << "couple_iteration = " << couple_iteration << endl;
		setCoupleVelocities(); // to measure the coupling
		//		THE FLUID CALCULATIONS (WITH AN OPTION FOR AN INTERNAL LOOP-"ITERATIONS")
		// internal iteration
		for (int in_iter=0; in_iter<p.internal_iterations; in_iter++) {
			for (int ix=1; ix<p.x_mesh-1; ix++) {
				for (int iy=1; iy<p.y_mesh-1; iy++) { //@@
					for (int iz=1; iz<p.z_mesh-1; iz++) { // @@
						fieldCalculation(ix, iy, iz);
					}
				}
			}
			// DRAG FORCES: METHOD 1-USE THIS ONE
			dragForces();
			updateInteractions();
			// CONTACT-LIKE INTERACTION WITH WALL <--- Not yet
			updateVelocityPositionInIteration();
			total_iteration++;
		}
		adaptiveTimestep();
	}
	enlargeTimestep();
}

void System::determineTimestep()
{
	// THE VARIOUS WAVE SPEEDS FOR CFL TO DETERMINE THE TIME STEPS
	//double c = sqrt((2*p.mu2/3)/p.rho_particle);
	double c_max;
	double cc_liquid = sqrt((2*p.mu_liquid/3)/p.rho_liquid);
	double cc_2      = sqrt((2*p.mu2      /3)/p.rho2);
	if (cc_liquid > cc_2) {
		c_max = cc_liquid;
	} else {
		c_max = cc_2;
	}
	//	total_time = p.time_extension*lx_half/c_max; //@@@ Why??
	total_time = 3000;
	cerr << "total_time = " << total_time << endl;
	delta_time = p.majic_scale*dx_mesh/c_max;
	cerr << "delta_time = " << delta_time << endl;
	delta_frames = total_time/p.movie_frames; // @@should be changed
	delta_time0    = delta_time;
	delta_time_max = delta_time0*p.max_factor;
	delta_time_min = delta_time0*p.min_factor;
}

void System::adaptiveTimestep()
{
	couple_diff_norm = 0;
	double couple_norm = 0;
	double couple_norm4 = 0;
	double couple_norm5 = 0;
	couple_diff_norm4 = 0;
	couple_diff_norm5 = 0;
	//NORM 1 OPTION (MAX OF THE NORMS (NORMALIZED BY THE SOL.))
	if (p.norm_option == 1) {
		for (int ix=1; ix<p.x_mesh-1; ix++) {
			for (int iy=1; iy<p.y_mesh-1; iy++) {
				for (int iz=1; iz<p.z_mesh-1; iz++) {
					couple_norm4 += flow_velocity_t_p_dt[ix][iy][iz].sq_norm();
					couple_diff_norm4 += (flow_velocity_t_p_dt[ix][iy][iz]-flow_velocity_couple[ix][iy][iz]).sq_norm();
				}
			}
		}
		for (int i=0; i<num_particle; i++) {
			couple_norm5 += particle_velocity_t_p_dt[i].sq_norm();
			couple_diff_norm5 += (particle_velocity_t_p_dt[i]-particle_velocity_couple[i]).sq_norm();
		}
		cerr << "couple_diff_norm4 = " << couple_diff_norm4 << endl;
		cerr << "couple_diff_norm5 = " << couple_diff_norm5 << endl;
		couple_norm = p.coupling_weight_flow*couple_norm4+p.coupling_weight_particle*couple_norm5;
		couple_diff_norm = p.coupling_weight_flow*couple_diff_norm4+p.coupling_weight_particle*couple_diff_norm5;
		couple_diff_norm = sqrt(couple_diff_norm);
		//		if (couple_norm > 0) {
		//			couple_diff_norm = sqrt(couple_diff_norm/couple_norm);
		//		} else {
		//			couple_diff_norm = 0;
		//		}
	} else {
		cerr << "p.norm_option != 1 are not implemented yet." << endl;
		exit(1);
	}
	// EXTRA OPTION
	o_flag = false;
	if (p.min_override) {
		double degree_of_freedom = p.coupling_weight_flow*p.x_mesh*p.y_mesh*p.z_mesh+p.coupling_weight_particle*num_particle;
		if (sqrt(couple_norm/degree_of_freedom) <= p.min_tolerance*radius_0/delta_time_max) {
			double measure = 0;
			couple_diff_norm = measure;
			o_flag = 1;
			cerr << " What is this part means?" << endl;
			//exit(1);
		}
	}
	cerr << "***************************************\n";
	cerr << "time and global coupled iteration = " << time/total_time << " and " << couple_iteration << endl;
	cerr << "ratio of new delta_time / original delta_t = " << delta_time / delta_time0 << endl;
	cerr << "***************************************\n";
	cerr << "couple_diff_norm = " << couple_diff_norm << endl;
	cerr << "couple_diff_norm / couple_tolerance " << couple_diff_norm / p.couple_tolerance << endl;
	if (couple_iteration == 1) {
		err0start = couple_diff_norm;
	}
	reductionTimestep();
}

void System::reductionTimestep()
{
	if (couple_iteration >= p.couple_iteration_limit && abs(couple_diff_norm) >= p.couple_tolerance) {
		double scale;
		time -= delta_time;
		if (o_flag) {
			scale = p.max_scale;
		} else {
			double exponent = 1.0/(p.expo*(couple_iteration-1));
			cout << "R" << couple_iteration << ' ' << exponent << endl;
			scale = pow(p.couple_tolerance/err0start, const_ts1)/pow(couple_diff_norm/err0start, exponent);
		}
		if (scale > p.max_scale) {
			scale = p.max_scale;
		} else if (scale < p.max_scale) {
			scale = p.min_scale;
		}
		delta_time *= scale;
		if (delta_time > delta_time_max) {
			delta_time = delta_time_max;
		} else if (delta_time < delta_time_min) {
			delta_time = delta_time_min;
		}
		time += delta_time;
		if (time < 0) {
			time = 0;
		}
		couple_diff_norm = 10*p.couple_tolerance;
		couple_iteration = 0;
		a_flag = 0;
	}
}

void System::enlargeTimestep()
{
	double scale;
	if (a_flag >= p.readapt
		&& couple_iteration <= p.couple_iteration_limit
		&& abs(couple_diff_norm) <= p.couple_tolerance) {
		if (o_flag) {
			scale = p.max_scale;
		} else {
			double exponent = 1.0/(p.expo*(couple_iteration-1));
			scale = pow(p.couple_tolerance/err0start, const_ts1)/pow(couple_diff_norm/err0start, exponent);
			// couple_diff_norm <-- this value is very small
			cout << "E" << couple_iteration << " " << exponent << " " << scale << endl;
			cout << p.couple_tolerance << ' ' << couple_diff_norm << ' ';
			cout <<  "  " << pow(p.couple_tolerance/err0start, const_ts1) << " " << pow(couple_diff_norm/err0start, exponent) << endl;
		}
		if (scale > p.max_scale) {
			scale = p.max_scale;
		}
		if (scale < 1) {
			scale = 1;
		}
		delta_time *= scale;
		if (delta_time > delta_time_max) {
			delta_time = delta_time_max;
		} else if (delta_time < delta_time_min) {
			// Is this required?
			delta_time = delta_time_min;
		}
		a_flag = 0;
		couple_iteration = 0;
		cerr << "UNREFINING TIME STEP DELTAT= " << delta_time << endl;
		cerr << "RATIO OF NEW DELTAT/ORIGINAL DELTAT=" << delta_time/delta_time0 << endl;
	}
}

void System::setConfiguration(const std::vector <vec3d>& initial_positions,
							  const std::vector <double>& radius_,
							  double lx_, double ly_, double lz_)
{
	/**
		\brief Set positions of the particles for initialization.
	 */
	std::string indent = "  System::\t";
	num_particle = (int)initial_positions.size();
	lx = lx_*p.geometrical_scaling;
	ly = ly_*p.geometrical_scaling;
	lz = lz_*p.geometrical_scaling;
	lx_half = lx/2;
	ly_half = ly/2;
	lz_half = lz/2;
	particle_position_t.resize(num_particle);
	particle_position_t_p_dt.resize(num_particle);
	radius.resize(num_particle);
	for (int i=0; i<num_particle; i++) {
		particle_position_t[i] = p.geometrical_scaling*initial_positions[i];
		particle_position_t_p_dt[i] = p.geometrical_scaling*initial_positions[i];
		radius[i] = p.geometrical_scaling*radius_[i];
	}
	radius_0 = radius[0]; //// @@@ Monodispese is assumed
	initializeBoxing();
	
	test_particle = 0;
	vec3d pos_middle(lx_half, ly_half, lz_half);
	double min_sq_dist_center = (pos_middle-particle_position_t[0]).sq_norm();
	for (int i=0; i<num_particle; i++) {
		if (min_sq_dist_center > (pos_middle-particle_position_t[i]).sq_norm()) {
			min_sq_dist_center = (pos_middle-particle_position_t[i]).sq_norm();
			test_particle = i;
		}
	}
}

void System::checkNewInteraction(std::vector<vec3d> &position_)
{
	/**
	 \brief Checks if there are new pairs of interacting particles. If so, creates and sets up the corresponding Interaction objects.
	 
	 To be called after particle moved.
	 */
	vec3d pos_diff;
	double nn_cutoff_dist = p.neighbor_cutoff_dist*radius_0;
	double sq_dist_lim = nn_cutoff_dist*nn_cutoff_dist;
	for (int i=0; i<num_particle; i++) {
		for (const int& j : boxset.neighborhood(i)) {
			if (j > i) {
				if (interaction_partners[i].find(j) == interaction_partners[i].end()) {
					pos_diff = position_[j]-position_[i];
					periodize_diff(pos_diff);
					if (pos_diff.sq_norm() < sq_dist_lim) {
						createNewInteraction(i, j, nn_cutoff_dist);
					}
				}
			}
		}
	}
}

void System::createNewInteraction(int i, int j, double scaled_interaction_range)
{
	int interaction_new;
	if (deactivated_interaction.empty()) {
		// add an interaction object.
		interaction_new = num_interaction;
		num_interaction ++;
	} else {
		// fill a deactivated interaction object.
		interaction_new = deactivated_interaction.front();
		deactivated_interaction.pop();
	}
	// new interaction
	if (num_interaction >= maxnum_interaction) {
		throw std::runtime_error("Too many interactions.\n");
		
	}
	interaction[interaction_new].activate(i, j, scaled_interaction_range);
}

void System::initializeBoxing()
{
	/**
		\brief Initialize the boxing system.
	 
		Initialize the BoxSet instance using as a minimal Box size the maximal interaction range between any two particles in the System.
	 */
	double max_range = p.interaction_cutoff*p.particle_radius;//@@@ to be checked
	boxset.init(max_range, this);
	for (int i=0; i<num_particle; i++) {
		boxset.box(i);
	}
	boxset.update();
}

// [0,l]
int System::periodize(vec3d& pos)
{
	if (pos.x >= lx) {
		pos.x -= lx;
		return 1;
	} else if (pos.x < 0) {
		pos.x += lx;
	}
	if (pos.y >= ly) {
		pos.y -= ly;
		
	} else if (pos.y < 0) {
		pos.y += ly;
	}
	if (pos.z >= lz) {
		pos.z -= lz;
	} else if (pos.z < 0) {
		pos.z += lz;
	}
	return 0;
}

void System::periodize_diff(vec3d& pos_diff)
{
	if (pos_diff.z > lz_half) {
		pos_diff.z -= lz;
	} else if (pos_diff.z < -lz_half) {
		pos_diff.z += lz;
	}
	if (pos_diff.x > lx_half) {
		pos_diff.x -= lx;
	} else if (pos_diff.x < -lx_half) {
		pos_diff.x += lx;
	}
	if (pos_diff.y > ly_half) {
		pos_diff.y -= ly;
	} else if (pos_diff.y < -ly_half) {
		pos_diff.y += ly;
	}
}

void System::outputParticle(std::ofstream &fout)
{
	static bool first = true;
	if (!first) {
		fout << endl;
	} else {
		first = false;
	}
	fout << "y 1" << endl;
	fout << "r " << radius[0]/p.geometrical_scaling << endl;
	for (int i=0; i<num_particle; i++) {
		if ( i == test_particle) {
			fout << "@ 3" << endl;
		} else {
			fout << "@ 2" << endl;
		}
		fout << "c " << (particle_position_t[i].x-lx_half)/p.geometrical_scaling;
		fout << ' '  << (particle_position_t[i].y-ly_half)/p.geometrical_scaling;
		fout << ' '  << (particle_position_t[i].z-lz_half)/p.geometrical_scaling;
		fout << endl;
	}
	fout << "y 3" << endl;
	fout << "@ 3" << endl;
	fout << "c " << (particle_position_t[test_particle].x-lx_half)/p.geometrical_scaling;
	fout << ' '  << (particle_position_t[test_particle].y-ly_half)/p.geometrical_scaling;
	fout << ' '  << (particle_position_t[test_particle].z-lz_half)/p.geometrical_scaling;
	fout << endl;
	fout << "y 2" << endl;
	fout << "@ 4" << endl;
	vec3d pos;
	vec3d pos_end;
	int iy_mid = 0.5*p.y_mesh;
	for (int ix = 0; ix < p.x_mesh; ix++){
		pos.x = ix*dx_mesh-lx_half;
		for (int iy = iy_mid; iy < iy_mid+1; iy++){
			pos.y = iy*dy_mesh-ly_half;
			for (int iz = 0; iz < p.z_mesh; iz++){
				pos.z = iz*dz_mesh-lz_half;
				vec3d pos_scale = (1.0/p.geometrical_scaling)*pos;
				//vec3d flowvel = (flow_velocity_t[ix][iy][iz]-velocity_face_list[0])/p.inflow_velocity;
				vec3d flowvel = flow_velocity_t[ix][iy][iz]/p.inflow_velocity;
				//if (flowvel.norm() > 0.0001) {
				pos_end = pos_scale+0.5*(dx_mesh/p.geometrical_scaling)*flowvel;
				fout << "l " << pos_scale << " " << pos_end << endl;
				//}
			}
		}
	}
	fout << "y 3" << endl;
	fout << "@ 0" << endl;
	fout << "r " << 0.01 << endl;
	for (const auto &ip : particle_domain) {
		pos.x = ip.ix*dx_mesh-lx_half;
		pos.y = ip.iy*dy_mesh-ly_half;
		pos.z = ip.iz*dz_mesh-lz_half;
		fout << "c " << pos.x/p.geometrical_scaling;
		fout << ' '  << pos.y/p.geometrical_scaling;
		fout << ' '  << pos.z/p.geometrical_scaling;
		fout << endl;
		
	}
	
}

void System::outputFlow(std::ofstream &fout)
{
	int iy = (int)(0.5*p.x_mesh);
	vec3d pos;
	pos.y = iy*dy_mesh-ly_half;
	for (int ix = 0; ix < p.x_mesh; ix++){
		pos.x = ix*dx_mesh-lx_half;
		for (int iz = 0; iz < p.z_mesh; iz++){
			pos.z = iz*dz_mesh-lz_half;
			//fout << rho_t[ix][iy][iz] << ' ';
			fout << mu_t[ix][iy][iz] << ' ';
		}
		fout << endl;
	}
}

bool System::in_range(int ix, int iy, int iz)
{
	if (ix < 0 || ix >= p.x_mesh || iy < 0 || iy >= p.y_mesh || iz < 0 || iz >= p.z_mesh) {
		return false;
	} else {
		return true;
	}
}

void System::allocate3Dbox_vec3d(vec3d ***&variable, int xsize, int ysize, int zsize)
{
	variable = new vec3d **[xsize];
	variable[0] = new vec3d *[xsize*ysize];
	variable[0][0] = new vec3d [xsize*ysize*zsize];
	if (variable[0][0]) {
		for (int ix=0; ix<xsize; ix++) {
			variable[ix] = variable[0]+ix*ysize;
			for (int iy=0; iy<ysize; iy++) {
				variable[ix][iy] = variable[0][0]+ix*ysize*zsize+iy*zsize;
			}
		}
	} else {
		exit(1);
	}
}

void System::allocate3Dbox_double(double ***&variable, int xsize, int ysize, int zsize)
{
	variable = new double **[xsize];
	variable[0] = new double *[xsize*ysize];
	variable[0][0] = new double [xsize*ysize*zsize];
	if (variable[0][0]) {
		for (int ix=0; ix<xsize; ix++) {
			variable[ix] = variable[0]+ix*ysize;
			for (int iy=0; iy<ysize; iy++) {
				variable[ix][iy] = variable[0][0]+ix*ysize*zsize+iy*zsize;
			}
		}
	} else {
		exit(1);
	}
}

void System::allocate3Dbox_stress(StressTensor ***&variable, int xsize, int ysize, int zsize)
{
	variable = new StressTensor **[xsize];
	variable[0] = new StressTensor *[xsize*ysize];
	variable[0][0] = new StressTensor [xsize*ysize*zsize];
	if (variable[0][0]) {
		for (int ix=0; ix<xsize; ix++) {
			variable[ix] = variable[0]+ix*ysize;
			for (int iy=0; iy<ysize; iy++) {
				variable[ix][iy] = variable[0][0]+ix*ysize*zsize+iy*zsize;
			}
		}
	} else {
		exit(1);
	}
}

System::~System()
{
	DELETE(flow_velocity_t[0][0]);
	DELETE(flow_velocity_t[0]);
	DELETE(flow_velocity_t);
	DELETE(flow_velocity_t_p_dt[0][0]);
	DELETE(flow_velocity_t_p_dt[0]);
	DELETE(flow_velocity_t_p_dt);
	DELETE(flow_velocity_couple[0][0]);
	DELETE(flow_velocity_couple[0]);
	DELETE(flow_velocity_couple);
	DELETE(gridforces_t[0][0]);
	DELETE(gridforces_t[0]);
	DELETE(gridforces_t);
	if (p.uniform_rho == false) {
		DELETE(grad_rho_t[0][0]);
		DELETE(grad_rho_t[0]);
		DELETE(grad_rho_t);
		DELETE(grad_rho_t_p_dt[0][0]);
		DELETE(grad_rho_t_p_dt[0]);
		DELETE(grad_rho_t_p_dt);
		DELETE(rho_initial[0][0]);
		DELETE(rho_initial[0]);
		DELETE(rho_initial);
		DELETE(rho[0][0]);
		DELETE(rho[0]);
		DELETE(rho);
		DELETE(rho_t[0][0]);
		DELETE(rho_t[0]);
		DELETE(rho_t);
		DELETE(rho_t_p_dt[0][0]);
		DELETE(rho_t_p_dt[0]);
		DELETE(rho_t_p_dt);
	}
	if (p.uniform_mu == false) {
		DELETE(mu[0][0]);
		DELETE(mu[0]);
		DELETE(mu);
		DELETE(mu_t[0][0]);
		DELETE(mu_t[0]);
		DELETE(mu_t);
		DELETE(mu_t_p_dt[0][0]);
		DELETE(mu_t_p_dt[0]);
		DELETE(mu_t_p_dt);
		DELETE(mu_initial[0][0]);
		DELETE(mu_initial[0]);
		DELETE(mu_initial);
	}
	DELETE(qf[0][0]);
	DELETE(qf[0]);
	DELETE(qf);
	DELETE(qf_initial[0][0]);
	DELETE(qf_initial[0]);
	DELETE(qf_initial);
	DELETE(pressure_t[0][0]);
	DELETE(pressure_t[0]);
	DELETE(pressure_t);
	DELETE(pressure_t_p_dt[0][0]);
	DELETE(pressure_t_p_dt[0]);
	DELETE(pressure_t_p_dt);
	DELETE(lambda_t[0][0]);
	DELETE(lambda_t[0]);
	DELETE(lambda_t_p_dt[0][0]);
	DELETE(lambda_t_p_dt[0]);
	DELETE(lambda_t_p_dt);
	DELETE(stress_t_p_dt[0][0]);
	DELETE(stress_t_p_dt[0]);
	DELETE(stress_t_p_dt);
	DELETE(sym_velocity_grad_t_p_dt[0][0]);
	DELETE(sym_velocity_grad_t_p_dt[0]);
	DELETE(sym_velocity_grad_t_p_dt);
	DELETE(interaction);
	DELETE(interaction_list);
	DELETE(interaction_partners);
}
