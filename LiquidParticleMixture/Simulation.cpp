//
//  Simulation.cpp
//
//  Created by Ryohei Seto on 3/17/16.
//  Copyright © 2016 T. I. ZOHDI, Ryohei Seto. All rights reserved.
//

#include "Simulation.hpp"
using namespace std;

Simulation::Simulation():
sys(System(p, events)),
kill(false)
{
	indent = "  Simulation::\t";
};

Simulation::~Simulation()
{
	fout_particle.close();
	fout_flow.close();
	fout_data.close();
	fout_rho.close();
};

bool Simulation::keepRunning()
{
	/** \brief Determine if we reached the end of the simulation.
	 
		Returns true when ParameterSet::time_end is reached or if an event handler threw a kill signal.
	 */
	return (sys.time < time_end-1e-8) && !kill;
}


void Simulation::simulationSet1(std::string in_args,
								std::vector<std::string>& input_files)
{
	
	setupSimulation(in_args, input_files);
	sys.setupSystem();
	time_end = 10000; //@@@
	///////////////////////////////////////////////
	sys.determineTimestep();
	sys.assigneMaterialProperties();
	sys.setInitialCondition();
	sys.err0start = 1000000; // @@ What is this?
	sys.a_flag = 0; // What is this?
	double delta_time = 1;
	double next_time = 0;
	while (keepRunning()) {
		fout_data << sys.time << ' ' << sys.delta_time << ' ' << sys.couple_diff_norm4 << ' ' << sys.couple_diff_norm5 << ' ';
		fout_data << sys.particle_velocity_t[sys.test_particle].x << ' ' <<sys.particle_position_t[sys.test_particle].x << ' ';
		fout_data << sys.drag_coeff_search << ' ';
		fout_data << sys.value_check[0] << ' ';
		fout_data << sys.value_check[2] << endl;
		sys.couple_diff_norm = 10*p.couple_tolerance;
		sys.couple_iteration = 0;
		sys.a_flag ++;
		sys.checkNewInteraction(sys.particle_position_t);
		sys.applyFlowBoundaryCondition();
		sys.internalCouplingInteractions();
		sys.update();
		sys.time += sys.delta_time;
		sys.t_count ++;
		if (sys.time > next_time) {
			next_time += delta_time;
			outputYaplot();
		}
	}
	cerr << "complete" << endl;
}

void Simulation::setupSimulation(string in_args,
								 vector<string>& input_files)
{
	string filename_parameters = input_files[0];
	string filename_import_positions = input_files[1];
	setDefaultParameters();
	readParameterFile(filename_parameters);
	importConfiguration(filename_import_positions);
	fout_particle.open("particle.yap");
	fout_flow.open("flow.yap");
	fout_data.open("tmp.dat");
	fout_rho.open("rho.dat");
	
}

void Simulation::importConfiguration(const string& filename_import_positions)
{
	/**
	 \brief Read a text file input configuration.
	 */
	fstream file_import;
	file_import.open(filename_import_positions.c_str());
	if (!file_import) {
		ostringstream error_str;
		error_str  << " Position file '" << filename_import_positions << "' not found." <<endl;
		throw runtime_error(error_str.str());
	}
	double lx, ly, lz;
	getline(file_import, header_imported_configulation[0]);
	getline(file_import, header_imported_configulation[1]);
	map<string,string> meta_data = getConfMetaData(header_imported_configulation[0], header_imported_configulation[1]);
	string key, def;
	key = "lx";
	lx = atof(getMetaParameter(meta_data, key).c_str());
	key = "ly";
	ly = atof(getMetaParameter(meta_data, key).c_str());
	key = "lz";
	lz = atof(getMetaParameter(meta_data, key).c_str());
	key = "vf1";
	sys.volume_fraction = atof(getMetaParameter(meta_data, key).c_str());
	vector<vec3d> initial_position;
	vector <double> radius;
	double x_, y_, z_, a_;
	string line;
	while (getline(file_import, line)) {
		istringstream is;
		is.str(line);
		is >> x_ >> y_ >> z_ >> a_;
		initial_position.push_back(vec3d(x_, y_, z_));
		radius.push_back(a_);
	}
	p.geometrical_scaling = p.particle_radius/radius[0];
	sys.setConfiguration(initial_position, radius, lx, ly, lz);
	file_import.close();
}

void Simulation::setDefaultParameters()
{
	/**
	 \brief Set default values for ParameterSet parameters.
	 */
	p.uniform_rho = true;
	p.uniform_mu = true;
	p.min_tolerance = 0.01;
	p.norm_option = 1;
	p.min_override = true;
	p.couple_tolerance = 0.001;
	p.geometrical_scaling = 1; // @@ Need to be changed
	p.motionless = true; // Is this just for initial condition?
	p.max_factor = 10; // relate to time steping
	p.min_factor = 0.00001; // relate to time steping
	p.internal_iterations = 1; // Why internal_iterations = 1 is used?
	p.min_scale = 0.99; // relate to time steping
	p.max_scale = 1.01; // relate to time steping
	p.phi = 0.5;
	p.coupling_weight_flow = 0.5;
	p.coupling_weight_particle = 0.5;
	p.couple_iteration_limit = 10;
	p.readapt = 2;
	p.expo = 1;
	p.x_mesh = 50;
	p.y_mesh = 50;
	p.z_mesh = 50;
	// FAC = 1 ---> scaling stress data output(?). What is its role?
	p.movie_frames = 50;
	// PERCENTAGE1=0.75D0  <--- Just for visualization?
	// PERCENTAGE2=0.75D0
	// PERCENTAGE3=0.75D0
	p.charge_option = 2; // not used yet
	p.imcompressible = true;
	// FASTSEARCH=1
	// CHECKFREQ=0.01D0
	p.neighbor_cutoff_dist = 4;
	p.drag_method = 5;
	p.coeff_drag = 0.3; // Used when drag_method = 1 or 3.
	p.damping = 0;
	p.pressure0 = 0;
	p.time_extension = 5;
	p.inflow_velocity = 0.01;
	p.outflow_velocity = p.inflow_velocity;
	p.profile = 2; // this is not effective when off = 0;
	p.off = 0;
	p.side_periodic = true; // Only periodic boundary is implemented so far.
	p.interparticle_interaction_A1 = 0;
	p.interparticle_interaction_A2 = 0;
	p.interparticle_interaction_B1 = -1;
	p.interparticle_interaction_B2 = -2;
	p.contact_force_stiffness = 10; // B3
	p.contact_force_exponent = 1; // B3
	// p.volume_fraction From the initial configration file.
	p.decay_x = 0; // screening of electric force?
	p.decay_y = 0;
	p.decay_z = 0;
	p.interaction_cutoff = 4; // not used yet.
	p.particle_radius = 0.05; //
	p.majic_scale = 0.01;
	// MAJICSCALE=0.01D0
	p.wall_stiffness = 0; // not implemented yet.
	p.mu_sens = 0;
	p.q_sens = 0;  // not implemented yet
	p.friccoeff_dynamics = 0.2; // not implemented yet
	p.friccoeff_static = 0.3; // not implemented yet
	p.contact_comp = 0.01; // not implemented yet
	//////////////////////////////////////
	// CHARGE PER UNIT VOLUME
	//////////////////////////////////////
	p.q_particle = 1;
	p.q_liquid = 0;
	p.q2 = 0;
	//	ESOURCEX1O=0.0D0
	//	ESOURCEX2O=0.0D0
	//	ESOURCEX3O=1.0D0
	//	MSOURCEX1O=0.0D0
	//	MSOURCEX2O=0.0D0
	//	MSOURCEX3O=1000.0D0
	//	MAGNETIZEP=0.0D0
	//	MAGNETIZEF=0.0D0
	double rho_ = 1000;
	p.rho_particle = rho_;
	//////////////////////////////////////
	// MATERIAL PARAMETERS
	//////////////////////////////////////
	p.mu_liquid = 0.1;
	p.rho_liquid = rho_;
	p.mu2 = 0.1;
	p.rho2 = rho_;
	///////////////////
}

void Simulation::autoSetParameters(const string &keyword, const string &value)
{
	/**
	 \brief Parse an input parameter
	 */
	string numeral, suffix;
	if (keyword == "foo") {
		// p.foo = atoi(value.c_str());
	} else if (keyword == "bar") {
		//p.bar = str2bool(value);
	} else {
		ostringstream error_str;
		error_str  << "keyword " << keyword << " is not associated with an parameter" << endl;
		throw runtime_error(error_str.str());
	}
}

void Simulation::readParameterFile(const string& filename_parameters)
{
	/**
	 \brief Read and parse the parameter file
	 */
	ifstream fin;
	fin.open(filename_parameters.c_str());
	if (!fin) {
		ostringstream error_str;
		error_str  << " Parameter file '" << filename_parameters << "' not found." <<endl;
		throw runtime_error(error_str.str());
	}
	string keyword, value;
	while (!fin.eof()) {
		string line;
		if (!getline(fin, line, ';')) {
			break;
		}
		if (fin.eof()) {
			break;
		}
		string str_parameter;
		removeBlank(line);
		str_parameter = line;
		string::size_type begin_comment;
		string::size_type end_comment;
		do {
			begin_comment = str_parameter.find("/*");
			end_comment = str_parameter.find("*/");
			if (begin_comment > 10000) {
				break;
			}
			str_parameter = str_parameter.substr(end_comment+2);
		} while (true);
		if (begin_comment > end_comment) {
			cerr << str_parameter.find("/*") << endl;
			cerr << str_parameter.find("*/") << endl;
			throw runtime_error("syntax error in the parameter file.");
		}
		string::size_type pos_slashslash = str_parameter.find("//");
		if (pos_slashslash != string::npos) {
			throw runtime_error(" // is not the syntax to comment out. Use /* comment */");
		}
		Str2KeyValue(str_parameter, keyword, value);
		autoSetParameters(keyword, value);
	}
	fin.close();
	return;
}

void Simulation::outputYaplot(){
	sys.outputParticle(fout_particle);
	//sys.outputFlow(fout_flow);
	//sys.outputFlow(fout_rho);
}

string Simulation::getMetaParameter(map<string,string> &meta_data, string &key)
{
	if (meta_data.find(key) != meta_data.end()) {
		return meta_data[key];
	} else {
		ostringstream error_str;
		error_str  << " Simulation:: parameter '" << key << "' not found in the header of the configuration file." <<endl;
		throw runtime_error(error_str.str());
	}
}

string Simulation::getMetaParameter(map<string,string> &meta_data, string &key, const string &default_val)
{
	if (meta_data.find(key) != meta_data.end()) {
		return meta_data[key];
	} else {
		return default_val;
	}
}

map<string,string> Simulation::getConfMetaData(const string &line1, const string &line2)
{
	vector<string> l1_split = splitString(line1);
	vector<string> l2_split = splitString(line2);
	if (l1_split.size() != l2_split.size()) {
		throw runtime_error("Simulation:: Ill-formed header in the configuration file.\n");
	}
	map<string,string> meta_data;
	for (unsigned int i=1; i<l1_split.size(); i++) {
		meta_data[l1_split[i]] = l2_split[i];
	}
	return meta_data;
}
